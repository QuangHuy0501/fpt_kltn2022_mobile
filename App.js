import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import Routes from './Components/Routes';
import Intro01 from './Components/Introduce/intro_01';
import Intro02 from './Components/Introduce/intro_02';
import Intro03 from './Components/Introduce/intro_03';
import About from './Components/AboutUs/about';
import Setting from './Components/AppSetting/setting';
import ResultHistory from './Components/History/resultHistory';
import InputGrade from './Components/Login/inputGrade';
import InputGPA from './Components/Login/inputGPA';
import Wait from './Components/Login/waitting';
import LoginPage from './Components/Login';
import ActiveUser from './Components/Login/active';
import ChooseTestPage from './Components/Loading/ChooseTestPage';
import PersonalResult from './Components/Result/personalResult';
import MajorResult from './Components/Result/majorResult';
import ResultHistoryDetail from './Components/History/resultHistoryDetail';
import EditProfile from './Components/Profile/Edit';
import ChooseColleges from './Components/Colleges/ChooseColleges';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Notificate from './Components/Home/noti';
import WishList from './Components/Colleges/WishList';
import Counseling from './Components/AdmissionsCounseling/counseling';
import AnimTab1 from './Components/Routes/test';
import HomeMainBoard from './Components/Home/homemain';
import CollegesDetail from './Components/Colleges/collegesDetail';
import Blog from './Components/Home/blog';
import BlogDetail from './Components/Home/blogDetail';
import NotificateChat from './Components/AdmissionsCounseling/notificateChat';
import CommentBlog from './Components/Home/commentBlog';
import LessonDetail from './Components/Home/lessonDetail';

export default function App() {
  return <Routes />;
}
