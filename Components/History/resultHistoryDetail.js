import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  BackHandler,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken, avtURL, userFullName} from '../Constant-Storage';
import API from '../API/api';
import Spiner from '../Loading/Spiner';

export default function ResultHistoryDetail({navigation, route}) {
  const [displayAvatar, setDisplayAvatar] = useState('');
  const [personGrName, setPersonGrName] = useState();
  const [majorNames, setMajorNames] = useState([]);
  const [displayUserName, setDisplayUserName] = useState('');
  const {nameTest, testID} = route.params;
  // const nameTest = 'MBTI';
  // const testID = 2;
  const [colleges, setColleges] = useState();

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.goBack();
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, [nameTest, testID]);

  function maxByKey(array) {
    let max = array[0];
    array.map((item, index) => {
      if (item.averagePoint > max.averagePoint) {
        max = item;
      }
    });
    return max;
  }
  useEffect(() => {
    const getPersonGroupName = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      api
        .onCallAPI(
          'get',
          `tests/result/${testID}`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(async res => {
          if (res.data) {
            console.log(res.data);
            const groupMax = maxByKey(res.data);
            const dtb = groupMax.averagePoint.toString();

            const diemTB = dtb.slice(0, 4);
            console.log(diemTB);
            groupMax.averagePoint = diemTB;
            console.log(groupMax);
            setPersonGrName(groupMax);
          }
        })
        .catch(err => {
          if (err.code === 'ERR_BAD_REQUEST') {
            Alert.alert('Thông tin cá nhân', "Don't have any result", [
              {
                text: 'OK',
                onPress: () =>
                  navigation.push('mainBoard', {
                    avtURL: '',
                    userName: '',
                  }),
              },
            ]);
          }
        });
    };

    async function userInfo() {
      const userAvatar = await AsyncStorage.getItem(avtURL);
      const userName = await AsyncStorage.getItem(userFullName);
      setDisplayAvatar(userAvatar);
      setDisplayUserName(userName);
    }
    getPersonGroupName();
    userInfo();
  }, []);
  useEffect(() => {
    const getMajor = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      if (personGrName)
        api
          .onCallAPI(
            'get',
            `majors/personality_group/${personGrName.id}`,
            {},
            {},
            {Authorization: 'bearer ' + tokenDnhap},
          )
          .then(res => {
            if (res.data) {
              var majorArr = '';
              res.data.map(item => {
                majorArr = majorArr + ',' + item.majorName;
              });
              const MajorNames = majorArr.slice(1, majorArr.length - 1);
              setMajorNames(MajorNames);
            }
          })
          .catch(err => alert(err));
    };
    getMajor();
  }, [personGrName]);
  if (personGrName && majorNames.length > 0)
    return (
      <View style={{flex: 1, position: 'relative', backgroundColor: '#ffa500'}}>
        <ImageBackground
          style={{height: 250, padding: 15}}
          source={require('../../assets/DrawerTopBanner.jpg')}>
          <TouchableOpacity
            style={{position: 'absolute', top: 10, left: 10}}
            onPress={() => navigation.goBack()}>
            <Ionicons name="arrow-back-outline" size={35} color="white" />
          </TouchableOpacity>
          {/* {
      //   "id": 22,
      //   "name": " ENFJ – Người cho ",
      //   "description": " Nhóm tính cách ENFJ rất ấm áp, tình cảm, dễ hòa hợp và luôn quan đến cảm nhận mọi người. Trẻ thuộc nhóm này có khả năng nhận thấy năng lực của người khác và chủ động giúp họ phát triển tiềm năng của mình. Đồng thời, trẻ có tác động quan trọng tới sự phát triển của cá nhân và cả nhóm. ",
      //   "averagePoint": 3.25
      // } */}
          <Image
            style={{
              width: 150,
              height: 150,
              borderRadius: 75,
              alignSelf: 'center',
            }}
            source={{
              uri: displayAvatar
                ? displayAvatar
                : 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
            }}
          />
          <Text
            style={{
              color: '#fff',
              fontSize: 25,
              fontFamily: 'Roboto-Medium',
              marginBottom: 5,
              alignSelf: 'center',
            }}>
            {displayUserName}
          </Text>
          <Text
            style={{
              color: '#fff',
              fontSize: 23,
              fontFamily: 'Roboto-Medium',
              marginBottom: 5,
              alignSelf: 'center',
            }}>
            Kết quả bài test {nameTest} của bạn
          </Text>
        </ImageBackground>
        <View
          style={{
            backgroundColor: 'white',
            borderTopStartRadius: 40,
            borderTopEndRadius: 40,
            flex: 1,
          }}>
          <View
            style={{
              height: 100,
              width: 100,
              borderRadius: 50,
              elevation: 50,
              shadowOffset: {width: 5, height: 5},
              shadowColor: 'black',
              shadowOpacity: 1,
              alignSelf: 'center',
              backgroundColor: 'white',
              marginTop: 20,
              justifyContent: 'center',
            }}>
            <Text style={{fontSize: 25, color: 'black', textAlign: 'center'}}>
              {personGrName?.averagePoint}
            </Text>
          </View>
          <Text
            style={{
              fontSize: 22,
              color: 'black',
              textAlign: 'center',
              marginTop: 10,
            }}>
            Là số điểm bạn đạt được
          </Text>
          <ScrollView style={{height: 200}}>
            <View style={{flex: 1, padding: 10}}>
              <Text
                style={{
                  fontSize: 22,
                  color: 'black',
                  marginTop: 20,
                  marginLeft: 20,
                }}>
                Bạn thuộc nhóm tính cách: {personGrName?.name}
              </Text>
              <Text
                style={{
                  fontSize: 22,
                  color: 'black',
                  marginTop: 10,
                  marginLeft: 20,
                }}>
                Ngành phù hợp với bạn: {majorNames}
              </Text>
            </View>
          </ScrollView>
          <View style={{}}></View>
        </View>
      </View>
    );
  return <Spiner />;
}
