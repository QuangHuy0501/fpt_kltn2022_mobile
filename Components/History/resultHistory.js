import {View, Text, TouchableOpacity, ScrollView, Image, BackHandler} from 'react-native';
import React, {useState} from 'react';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {SafeAreaView} from 'react-native-safe-area-context';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';

export default function ResultHistory({navigation}) {
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  return (
    <View style={{flex: 1, position: 'relative'}}>
      {/*design header */}
      <View style={{height: 150, justifyContent: 'center'}}>
        <SafeAreaView style={{}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 0.5, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() =>
                  navigation.push('mainBoard', {
                    avtURL: '',
                    userName: '',
                  })
                }
                style={{flex: 1}}>
                <Ionicons name="arrow-back-outline" size={32} color="#F07122" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 34,
                color: '#F07122',
                textAlign: 'center',
                paddingTop: 30,
              }}>
              Lịch sử kết quả
            </Text>
            <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
          </View>
        </SafeAreaView>
      </View>
      {/*design body trang */}
      {/*drop down */}
      {/*design scrollview ở body */}
      <View style={{flex: 4, marginVertical: 20}}>
        <ScrollView>
          <TouchableOpacity
            onPress={() =>
              navigation.push('ResultHistoryDetail', {
                nameTest: 'Holland',
                testID: '2',
              })
            }
            style={{
              width: '90%',
              height: 250,
              backgroundColor: 'white',
              borderRadius: 20,
              elevation: 20,
              shadowOffset: {width: 50, height: 50},
              shadowColor: 'black',
              shadowOpacity: 20,
              marginVertical: 10,
              alignSelf: 'center',
            }}>
            <View
              style={{
                flex: 1,
                marginHorizontal: 10,
                marginTop: 20,
                marginBottom: 10,
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 25, color: 'black', fontWeight: 'bold'}}>
                {' '}
                Kết quả Holland
              </Text>
            </View>
            <View
              style={{
                flex: 4,
                marginHorizontal: 10,
                marginBottom: 10,
                borderRadius: 15,
                backgroundColor: 'transparent',
              }}>
              <Image
                style={{
                  width: '100%',
                  height: '100%',
                  alignSelf: 'center',
                }}
                resizeMode="contain"
                source={require('../../assets/hollandResultHistory.jpg')}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              navigation.push('ResultHistoryDetail', {
                nameTest: 'MBTI',
                testID: '1',
              })
            }
            style={{
              width: '90%',
              height: 250,
              backgroundColor: 'white',
              borderRadius: 20,
              elevation: 20,
              shadowOffset: {width: 50, height: 50},
              shadowColor: 'black',
              shadowOpacity: 20,
              marginVertical: 10,
              alignSelf: 'center',
            }}>
            <View
              style={{
                flex: 1,
                marginHorizontal: 10,
                marginTop: 20,
                marginBottom: 10,
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 25, color: 'black', fontWeight: 'bold'}}>
                {' '}
                Kết quả MBTI
              </Text>
            </View>
            <View
              style={{
                flex: 4,
                marginHorizontal: 10,
                marginBottom: 10,
                borderRadius: 15,
                backgroundColor: 'transparent',
              }}>
              <Image
                style={{
                  width: '100%',
                  height: '100%',
                  alignSelf: 'center',
                }}
                resizeMode="contain"
                source={require('../../assets/mbtiResultHistory.jpg')}
              />
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </View>
  );
}
