import { View, Text, Image } from 'react-native'
import React from 'react'

export default function StartPage({navigation}) {
  return (
    <View style={{backgroundColor: 'white'}}>
      <Image
        source={require('../../assets/FPTLogo.png')}
        resizeMode="contain"
        style={{width:"100%", height:"100%"}}
      />
    </View>
  )
}