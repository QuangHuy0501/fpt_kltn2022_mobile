import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Linking,
  Alert,
  BackHandler,
} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import {useEffect} from 'react';
import {useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken} from '../Constant-Storage';
import API from '../API/api';
import Spiner from '../Loading/Spiner';
import {ScrollView} from 'react-native-gesture-handler';

export default function CollegesDetail({navigation, route}) {
  const {collegeId} = route.params;
  const [data, setdata] = useState(null);
  useEffect(() => {
    const getDetail = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      api
        .onCallAPI(
          'get',
          `colleges/${collegeId}/details`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            console.log(res.data);
            setdata(res.data);
          }
        })
        .catch(err => alert(err));
    };
    getDetail();
  }, []);
  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.goBack();
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  const handlePress = async supportedURL => {
    const supported = await Linking.canOpenURL(supportedURL);
    if (supported) {
      await Linking.openURL(supportedURL);
    } else {
      Alert.alert(`Don't know how to open this URL: ${supportedURL}`);
    }
  };
  if (collegeId && data) {
    return (
      <View style={{flex: 1, position: 'relative'}}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            height: 40,
            width: 40,
            backgroundColor: 'white',
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            position: 'absolute',
            left: 0,
            top: 0,
            justifyContent: 'center',
            alignItems: 'center',
            zIndex: 999,
          }}>
          <Feather name="arrow-left" size={35} color="orange" />
        </TouchableOpacity>
        <View>
          <Image
            style={{width: '100%', height: 250}}
            source={
              data.image.length > 0
                ? {uri: data.image}
                : require('../../assets/Intro01.jpg')
            }
          />
        </View>
        <View style={{paddingHorizontal: 10, paddingTop: 20}}>
          <View>
            <Text
              style={{
                textAlign: 'left',
                color: 'black',
                fontSize: 25,
                fontWeight: 'bold',
              }}>
              {data.name}
            </Text>
            <ScrollView style={{height: 200}}>
              <Text
                style={{
                  textAlign: 'left',
                  color: 'black',
                  fontSize: 20,
                  paddingTop: 15,
                }}>
                {data.detail}
              </Text>
            </ScrollView>
          </View>
          <View>
            <Text
              style={{
                textAlign: 'left',
                color: 'black',
                fontSize: 25,
                fontWeight: 'bold',
                paddingTop: 10,
              }}>
              Địa chỉ:
            </Text>
            <Text style={{color: 'black', fontSize: 20}}>
              {data.address}
              {/**data.address */}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingHorizontal: 10,
          }}>
          <View
            style={{
              justifyContent: 'center',
              height: 50,
              width: 160,
              marginTop: 20,
              borderRadius: 20,
              elevation: 50,
              shadowOffset: {width: 5, height: 5},
              shadowColor: 'black',
              shadowOpacity: 1,
              backgroundColor: '#ABD9FF',
            }}>
            <TouchableOpacity
              onPress={() => handlePress(data.refLink)}
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{color: 'black', fontSize: 20}}>Website trường</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              justifyContent: 'center',
              height: 50,
              width: 160,
              marginTop: 20,
              borderRadius: 20,
              elevation: 50,
              shadowOffset: {width: 5, height: 5},
              shadowColor: 'black',
              shadowOpacity: 1,
              backgroundColor: '#ABD9FF',
            }}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Counseling', {
                  modalView: true,
                  collegeId: collegeId,
                })
              }
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{color: 'black', fontSize: 20}}>Liên hệ tư vấn</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  return <Spiner />;
}
