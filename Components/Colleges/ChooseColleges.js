import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Linking,
  Alert,
} from 'react-native';
import React, {useState, useEffect, useCallback} from 'react';
import Entypo from 'react-native-vector-icons/Entypo';
import {SafeAreaView} from 'react-native-safe-area-context';
import API from '../API/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  currentStack,
  Current_Screen_Params,
  Grade_Arrays_Screen,
  indexQuestionMBTI,
  LoginAppToken,
  majorUserChoose,
  questionID_MBTI,
} from '../Constant-Storage';
import Spiner from '../Loading/Spiner';

export default function ChooseColleges({navigation, route}) {
  const [searchQuery, setSearchQuery] = React.useState('');
  // const {majorID} = route.params;
  // const majorID = 149;
  const onChangeSearch = query => setSearchQuery(query);
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [colleges, setColleges] = useState();
  console.log(colleges);
  const [keyboardStatus, setKeyboardStatus] = useState(false);
  const chooseColleges = async (collegesId, index) => {
    setLoading(true);
    const temp = [...colleges];
    if (temp[index].isSelected) {
      setLoading(false);
      return;
    }
    temp[index].isSelected = true; //!temp[index].IsSelected;

    setColleges(temp);
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    api
      .onCallAPI(
        'post',
        `sys_users/colleges`,
        {collegesId: collegesId},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(res => {
        if (res.data) {
          setLoading(false);
          Alert.alert('Thông báo', 'Đã lưu nguyện vọng', [{text: 'OK'}]);
        }
      })
      .catch(err => alert(err));
    setLoading(false);
  };

  useEffect(() => {
    const getColleges = async () => {
      await AsyncStorage.setItem(currentStack, 'UniversityScreen');

      const majorID = route.params?.majorID
        ? route.params.majorID
        : await AsyncStorage.getItem(majorUserChoose);
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `majors/${majorID}/Colleges`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            console.log(res.data);
            // res.data.map(item => (item.it = false));
            setColleges(res.data);
          }
        })
        .catch(err => alert(err));
    };
    getColleges();
  }, []);
  const handlePress = async supportedURL => {
    // supportedURL = 'http://' + supportedURL;
    const supported = await Linking.canOpenURL(supportedURL);
    if (supported) {
      await Linking.openURL(supportedURL);
    } else {
      Alert.alert(`Don't know how to open this URL: ${supportedURL}`);
    }
  };
  const homeButton = async () => {
    await AsyncStorage.setItem(currentStack, 'HomeScreen');
    await AsyncStorage.setItem(Current_Screen_Params, '');
    await AsyncStorage.setItem(currentStack, '');
    await AsyncStorage.setItem(Grade_Arrays_Screen, '');
    await AsyncStorage.setItem(indexQuestionMBTI, '');
    await AsyncStorage.setItem(questionID_MBTI, '');

    navigation.push('mainBoard', {
      avtURL: '',
      userName: '',
    });
  };
  const schoolrender = colleges => {
    return colleges.map((item, index) => {
      const supportedURL = item.referenceLink;
      console.log(item.isSelected);
      return (
        <View
          key={index}
          style={{
            flex: 1,
            flexDirection: 'row',
            backgroundColor: 'white',
            height: 200,
            borderRadius: 20,
            marginHorizontal: 10,
            marginBottom: 20,
            elevation: 20,
            shadowOffset: {width: 10, height: 10},
            shadowColor: 'black',
            shadowOpacity: 1,
          }}>
          <View
            style={{
              flex: 1,
              width: 80,
              height: 80,
              alignSelf: 'center',
              paddingLeft: 5,
            }}>
            <Image
              source={{uri: item.imagePath}}
              resizeMode="cover"
              style={{width: '100%', height: '100%', borderRadius: 40}}
            />
          </View>
          <View style={{flex: 4, paddingLeft: 10, paddingRight: 15}}>
            <View
              style={{
                flex: 2,
                flexDirection: 'row',
                borderBottomWidth: 1,
                borderBottomColor: 'grey',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={{flex: 4}}>
                <Text
                  style={{
                    fontSize: 22,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginRight: 30,
                  }}>
                  {' '}
                  {item.collegeName}{' '}
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => chooseColleges(item.collegeId, index)}
                style={{flex: 1, position: 'absolute', right: 0}}>
                <Entypo
                  name={item.isSelected ? 'star' : 'star-outlined'}
                  size={25}
                  color={item.isSelected ? 'yellow' : 'grey'}
                />
              </TouchableOpacity>
            </View>
            <View style={{flex: 1.5}}>
              <Text style={{fontSize: 18, textAlign: 'left'}}>
                {' '}
                Address: {item.address}
              </Text>
            </View>
            <TouchableOpacity
              style={{flex: 1}}
              key={index}
              onPress={() => handlePress(supportedURL)}>
              <Text style={{fontSize: 18, textAlign: 'left', color: '#ECA376'}}>
                {' '}
                Bấm vào đây để xem web trường
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };
  if (!colleges || loading) return <Spiner />;

  return (
    <View style={{flex: 1, position: 'relative'}}>
      {/*design phần header trang*/}
      <View style={{height: 190, justifyContent: 'center'}}>
        <SafeAreaView style={{}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 0.5, alignItems: 'flex-start'}}>
              {/* <TouchableOpacity
                onPress={() => navigation.toggleDrawer()}
                style={{flex: 1}}>
                <Entypo name="menu" size={32} color="#F07122" />
              </TouchableOpacity> */}
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 34,
                color: '#F07122',
                textAlign: 'center',
                paddingTop: 30,
              }}>
              DANH SÁCH TRƯỜNG
            </Text>
            <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
          </View>
          <View
            style={{
              width: '100%',
              paddingTop: 10,
              paddingHorizontal: 30,
            }}></View>
        </SafeAreaView>
      </View>
      {/*design phần thân dưới trang*/}
      <View style={{flex: 3}}>
        <Text
          style={{
            alignItems: 'flex-start',
            fontSize: 20,
            color: '#F07122',
            paddingLeft: 10,
            paddingBottom: 10,
          }}>
          Những trường phù hợp:
        </Text>
        {/*design phần scroll chọn trường*/}
        <ScrollView
          vertical
          style={{flex: 3, flexDirection: 'column', paddingVertical: 10}}>
          {schoolrender(colleges)}
        </ScrollView>
      </View>
      {/*design bottom bar*/}
      <View
        style={{
          justifyContent: 'center',
          alignSelf: 'center',
          height: 50,
          width: 250,
          marginVertical: 10,
          marginLeft: 15,
          borderRadius: 20,
          elevation: 50,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          backgroundColor: '#ABD9FF',
          display: keyboardStatus ? 'none' : 'flex',
        }}>
        <TouchableOpacity
          onPress={homeButton}
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{color: 'black', fontSize: 22}}>Hoàn thành bài test</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
