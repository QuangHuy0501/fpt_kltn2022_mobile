import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  BackHandler,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import API from '../API/api';
import {LoginAppToken} from '../Constant-Storage';
import Spiner from '../Loading/Spiner';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function WishList({navigation}) {
  const ScreenHeight = Dimensions.get('screen').height;
  const Screenwidth = Dimensions.get('screen').width;
  const [listWist, setListWish] = useState();

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    const getWishList = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `sys_users/colleges/wishlist`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            console.log(res.data);
            setListWish(res.data);
          }
        })
        .catch(err => alert(err));
    };
    getWishList();
  }, []);
  const wishListRender = listWist => {
    if (!listWist) return <Spiner />;
    return listWist.map((item, index) => {
      //const supportedURL = item.referenceLink;
      return (
        <View
          key={index}
          style={{
            backgroundColor: 'white',
            height: ScreenHeight * 0.4,
            marginVertical: 5,
            borderRadius: 20,
            elevation: 10,
            shadowOffset: {width: 10, height: 10},
            shadowColor: 'black',
            shadowOpacity: 1,
            marginHorizontal: 5,
            justifyContent: 'center',
          }}>
          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Image
                source={{
                  uri: item.imagePath,
                }}
                resizeMode="cover"
                style={{
                  height: 80,
                  width: 80,
                  borderRadius: 40,
                  marginLeft: 10,
                }}
              />
            </View>
            <View style={{flex: 2, marginRight: 10}}>
              <ScrollView style={{height: 200}}>
                <Text style={{fontSize: 20}}>{item.collegeName}</Text>
                <Text style={{fontSize: 20}}>
                  Ngành được giới thiệu: {''}
                  {item.major.map(name => {
                    console.log(name);
                    return (
                      <>
                        <Text key={'name' + index} style={{fontSize: 20}}>
                          {name.name} {'\n'}
                        </Text>
                      </>
                    );
                  })}
                </Text>
              </ScrollView>
            </View>
          </View>
        </View>
      );
    });
  };
  return (
    <View style={{flex: 1, position: 'relative'}}>
      {/*Design header*/}
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          height: 190,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          justifyContent: 'center',
        }}>
        <SafeAreaView style={{paddingBottom: 10}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 0.5, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{flex: 1}}>
                <Ionicons name="arrow-back-outline" size={32} color="white" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 32,
                color: 'white',
                textAlign: 'center',
                paddingTop: 30,
              }}>
              DANH SÁCH NGUYỆN VỌNG
            </Text>
            <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
          </View>
        </SafeAreaView>
      </LinearGradient>
      {/*Design body*/}
      <View
        style={{
          overflow: 'hidden',
          flex: 1,
          borderTopLeftRadius: 50,
          borderTopRightRadius: 50,
        }}>
        <ScrollView
          style={{
            backgroundColor: 'white',
            marginTop: 20,
            elevation: 20,
            shadowOffset: {width: 10, height: 10},
            shadowColor: 'black',
            shadowOpacity: 1,
            paddingHorizontal: 15,
          }}>
          {wishListRender(listWist)}
        </ScrollView>
      </View>
    </View>
  );
}
