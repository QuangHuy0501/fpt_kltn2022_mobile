import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React from 'react';
import {IntroShow} from '../Constant-Storage';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Intro03({navigation}) {
  const ScreenHeight = Dimensions.get('screen').height * 0.6;
  const ScreenWidth = Dimensions.get('screen').width;
  const removeIntro = async () => {
    let isIntro = await AsyncStorage.getItem(IntroShow);
    if (isIntro) {
      isIntro = parseInt(isIntro);
      await AsyncStorage.setItem(IntroShow, (isIntro + 1).toString());
    } else await AsyncStorage.setItem(IntroShow, '1');
    navigation.navigate('LoginPage');
  };
  return (
    <View style={{flex: 1, position: 'relative'}}>
      <Image
        style={{
          height: ScreenHeight,
          width: ScreenWidth,
          borderColor: '#00ff00',
          borderBottomRightRadius: 80,
          borderWidth: 5,
        }}
        resizeMode="cover"
        source={require('../../assets/Intro03.jpg')}
      />
      <ScrollView style={{marginTop: 10, marginHorizontal: 20}}>
        <Text
          style={{fontSize: 20, color: 'rgb(38, 38, 38)', fontFamily: 'arial'}}>
          Chúng tôi sẽ cung cấp cho bạn những dữ liệu chính xác nhất để giúp bạn
          hiểu rõ hơn về bản thân.
        </Text>
        <Text
          style={{fontSize: 20, color: 'rgb(38, 38, 38)', fontFamily: 'arial'}}>
          Hiểu được bản thân sẽ giúp bạn khám phá ra nhiểu khả năng mà trước giờ
          bạn chưa hề nghĩ tới. Cánh cửa đại học và có một công việc làm phù hợp
          cho tương lai chưa bao giờ dễ đến thế.{' '}
          <Text
            style={{
              fontSize: 20,
              color: 'rgb(38, 38, 38)',
              fontFamily: 'arial',
              fontWeight: 'bold',
            }}>
            Cùng bắt đầu thôi nào.
          </Text>
        </Text>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 20,
            justifyContent: 'space-between',
            paddingHorizontal: 20,
            flexWrap: 'wrap',
          }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{
              width: 120,
              height: 40,
              borderRadius: 20,
              backgroundColor: '#ECA376',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 20,
                color: '#fff',
                textAlign: 'center',
              }}>
              Previous
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={removeIntro}
            style={{
              width: 120,
              height: 40,
              borderRadius: 20,
              alignSelf: 'center',
              backgroundColor: '#ECA376',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 20,
                color: '#fff',
                textAlign: 'center',
              }}>
              Next
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}
