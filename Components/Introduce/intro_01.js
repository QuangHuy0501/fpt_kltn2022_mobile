import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import StartPage from '../StartPage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  avtURL,
  Current_Screen,
  indexQuestionMBTI,
  IntroShow,
  LoginAppToken,
  questionID_MBTI,
  userFullName,
} from '../Constant-Storage';
import API from '../API/api';
import {ActivityIndicator} from 'react-native-paper';
import Lottie from 'lottie-react-native';

export default function Intro01() {
  const ScreenHeight = Dimensions.get('screen').height * 0.6;
  const ScreenWidth = Dimensions.get('screen').width;
  const animationRef = useRef();
  // useEffect(() => {
  //   const backAction = async () => {
  //     await AsyncStorage.clear();
  //     return true;
  //   };
  //   ///can xoa sau khi test
  //   const backHandler = BackHandler.addEventListener(
  //     'hardwareBackPress',
  //     backAction,
  //   );

  //   return () => backHandler.remove();
  // }, []);

  useEffect(() => {
    const checkToken = async () => {
      const tokenApp = await AsyncStorage.getItem(LoginAppToken);
      const avatarLink = await AsyncStorage.getItem(avtURL);
      const userName = await AsyncStorage.getItem(userFullName);
      if (tokenApp && avatarLink && userName) {
        navigation.navigate('mainBoard', {
          avtURL: avatarLink,
          userName: userName,
        });
      }
    };
    checkToken();
  }, []);
  const navigation = useNavigation();
  const [loadingStatus, setLoadingStatus] = useState(true);
  useEffect(() => {
    const setIntro = async () => {
      const tokenApp = await AsyncStorage.getItem(LoginAppToken);
      let isIntro = await AsyncStorage.getItem(IntroShow);
      if (isIntro) isIntro = parseInt(isIntro);
      if (isIntro > 0) {
        if (!tokenApp) navigation.navigate('LoginPage');
        else {
          const avatarLink = await AsyncStorage.getItem(avtURL);
          const userName = await AsyncStorage.getItem(userFullName);
          navigation.push('mainBoard', {
            avtURL: avatarLink,
            userName: userName,
          });
        }
      }
    };
    setIntro();
  }, []);
  useEffect(() => {
    // animationRef.current?.play()
    // Or set a specific startFrame and endFrame with:
    animationRef.current?.play(30, 120);
  }, []);
  setTimeout(() => {
    setLoadingStatus(false);
    // setIntro();
  }, 10000);
  return loadingStatus ? (
    // <StartPage />
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Lottie
        ref={animationRef}
        source={require('../../assets/76212-student-transparent.json')}
      />
    </View>
  ) : (
    <View style={{flex: 1, position: 'relative'}}>
      <Image
        style={{
          height: ScreenHeight,
          width: ScreenWidth,
          borderBottomLeftRadius: 100,
          borderColor: '#00ff00',
          borderWidth: 5,
        }}
        resizeMode="cover"
        source={require('../../assets/Intro01.jpg')}
      />
      <ScrollView style={{marginTop: 10, marginHorizontal: 20}}>
        <Text
          style={{fontSize: 20, color: 'rgb(38, 38, 38)', fontFamily: 'arial'}}>
          Lựa chọn nghề nghiệp luôn là giai đoạn khó khăn khi chuẩn bị tốt
          nghiệp cấp 3.
        </Text>
        <Text
          style={{fontSize: 20, color: 'rgb(38, 38, 38)', fontFamily: 'arial'}}>
          Định hướng được đúng nghề nghiệp phù hợp với bạn sẽ giúp bạn cảm thấy
          dễ dàng hơn trong quá trình đại học và lựa chọn việc làm sau này.
        </Text>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 20,
            justifyContent: 'space-between',
            paddingHorizontal: 20,
            flexWrap: 'wrap',
          }}>
          <TouchableOpacity
            onPress={() => navigation.push('Intro02')}
            style={{
              width: 120,
              height: 40,
              borderRadius: 20,
              alignSelf: 'center',
              backgroundColor: '#ECA376',
              position: 'absolute',
              right: 10,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 20,
                color: '#fff',
                textAlign: 'center',
              }}>
              Next
            </Text>
          </TouchableOpacity>
          <View style={{height: 20}}></View>
        </View>
      </ScrollView>
    </View>
  );
}
