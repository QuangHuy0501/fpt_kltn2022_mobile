import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React from 'react';

export default function Intro02({navigation}) {
  const ScreenHeight = Dimensions.get('screen').height * 0.6;
  const ScreenWidth = Dimensions.get('screen').width;

  return (
    <View style={{flex: 1, position: 'relative'}}>
      <Image
        style={{
          height: ScreenHeight,
          width: ScreenWidth,
          borderColor: '#00ff00',
          borderWidth: 5,
        }}
        resizeMode="cover"
        source={require('../../assets/Intro02.jpg')}
      />
      <ScrollView style={{marginTop: 10, marginHorizontal: 20}}>
        <Text
          style={{fontSize: 20, color: 'rgb(38, 38, 38)', fontFamily: 'arial'}}>
          Các bài Test tính cách là một trong những phương tiện dự đoán hiệu
          suất chính xác nhất về khả năng của bản thân các bạn.
        </Text>
        <Text
          style={{fontSize: 20, color: 'rgb(38, 38, 38)', fontFamily: 'arial'}}>
          Trong nhiều loại công việc và ngành nghề, cần có mức độ kiến thức, kỹ
          năng và sự phù hợp. Bạn có thể có đầy đủ khả năng để đảm nhận
          những công việc như vậy? Hãy thử khám phá cùng chúng tôi.
        </Text>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 20,
            justifyContent: 'space-between',
            paddingHorizontal: 20,
            flexWrap: 'wrap',
          }}>
          <TouchableOpacity
            onPress={()=> navigation.goBack()}
            style={{
              width: 120,
              height: 40,
              borderRadius: 20,
              backgroundColor: '#ECA376',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 20,
                color: '#fff',
                textAlign: 'center',
              }}>
              Previous
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={()=>navigation.push('Intro03')}
            style={{
              width: 120,
              height: 40,
              borderRadius: 20,
              alignSelf: 'center',
              backgroundColor: '#ECA376',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 20,
                color: '#fff',
                textAlign: 'center',
              }}>
              Next
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}
