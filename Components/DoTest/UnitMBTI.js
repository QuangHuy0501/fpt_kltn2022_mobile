import {View, Dimensions, Text, TouchableOpacity, Alert} from 'react-native';
import React, {useEffect, useState, useRef} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LoginAppToken,
  indexQuestionMBTI,
  questionID_MBTI,
  Current_Screen,
  Current_Screen_Params,
  currentStack,
} from '../Constant-Storage';
import API from '../API/api';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import Spiner from '../Loading/Spiner';

export default function UnitMBTI({navigation, route}) {
  const ScreenHeight = Dimensions.get('screen').height;
  const [GroupName, setGroupName] = useState([]);
  const [dataRender, setdataRender] = useState();
  //useRef cũng để lưu tương tự useState nhưng ko render lại
  const numberQuestionShow = useRef();
  const {questionID, IDsMBTI} = route.params;
  const [finalResult, setFinalResult] = useState([]);
  const [answerChoose, setanswerChoose] = useState();

  useEffect(() => {
    const getQAmbti = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      await AsyncStorage.setItem(questionID_MBTI, questionID.toString());
      await AsyncStorage.setItem(Current_Screen, 'DoTest02');
      const index = await AsyncStorage.getItem(indexQuestionMBTI);
      const so = parseInt(index);
      setanswerChoose(undefined);
      numberQuestionShow.current = so;
      var temp = {};
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `questions/mbti/${questionID}`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            temp = {...res.data};
            // console.log(res.data);
          }
        })
        .catch(err => alert(err));
      api
        .onCallAPI(
          'get',
          `answers/question/${questionID}`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            temp = {...temp, answer: res.data};
            setdataRender(temp);
            console.log(res.data);
            // console.log('temp nè', temp);
          }
        })

        .catch(err => alert(err));
    };

    getQAmbti();
  }, [questionID]);

  const nextQuestion = async () => {
    if (numberQuestionShow.current !== undefined) {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const so = parseInt(numberQuestionShow.current) + 1;
      AsyncStorage.setItem(indexQuestionMBTI, so.toString());
      const data = {
        IDsMBTI: IDsMBTI,
        questionID: IDsMBTI[so],
      };
      if (!answerChoose) {
        Alert.alert('Thông báo', 'Bạn phải chọn câu trả lời', [
          {
            text: 'OK',
          },
        ]);
        return;
        return;
      }
      const api = new API();
      const temp = [...finalResult];
      temp.push(answerChoose);
      setFinalResult(temp);

      if (numberQuestionShow.current === 65) {
        await AsyncStorage.setItem(currentStack, 'personalityGrResult');
        const test = '1';
        await AsyncStorage.setItem(Current_Screen_Params, test);
        const params = [];
        temp.map((item, i) => {
          params.push(item.id);
        });
        api
          .onCallAPI(
            'post',
            `answers/result/${1}`,
            {listAnswerId: params},
            {},
            {Authorization: 'bearer ' + tokenDnhap},
          )
          .then(res => {
            if (res.data) {
              navigation.navigate('personalityGrResult', {
                testid: 1,
              });
            }
          })
          .catch(err => alert(err));
        return;
      }
      setdataRender(undefined);
      navigation.navigate('DoTest02', data);
    } else alert('Lỗi rồi ');
  };

  if (dataRender)
    return (
      <>
        {/*stt câu hỏi */}
        <Text style={{fontSize: 20, marginTop: 10, marginLeft: 10}}>
          {numberQuestionShow.current + 1}/66
        </Text>
        <View
          style={{
            backgroundColor: 'white',
            marginTop: 50,
            marginBottom: 30,
            marginHorizontal: 20,
            height: ScreenHeight * 0.15,
            borderRadius: 20,
            elevation: 10,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 20, textAlign: 'center'}}>
            {dataRender.content}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            marginBottom: 20,
            height: ScreenHeight * 0.45,
            elevation: 10,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            justifyContent: 'center',
          }}>
          {dataRender.answer.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                onPress={() =>
                  setanswerChoose({
                    id: item.answerId,
                    point: item.point,
                    personGroupid: item.personalityGroupId,
                  })
                }
                style={{
                  backgroundColor:
                    answerChoose?.id == item.answerId ? '#6495ed' : '#daa520',
                  marginTop: 10,
                  marginBottom: 10,
                  marginHorizontal: 20,
                  borderRadius: 40,
                }}>
                <Text
                  style={{
                    fontSize: 20,
                    padding: 10,
                    marginHorizontal: 10,
                    justifyContent: 'center',
                    textAlign: 'center',
                  }}>
                  {item.answerContent}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
        {/*nút next qua câu mới */}
        <TouchableOpacity
          onPress={nextQuestion}
          style={{
            height: 60,
            width: 250,
            backgroundColor: '#F07122D4',
            // position: 'absolute',
            // bottom: 20,
            // right: 20,
            borderRadius: 30,
            elevation: 10,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            justifyContent: 'center',
            alignSelf: 'center',
          }}>
          <Text
            style={{
              fontSize: 25,
              color: 'white',
              textAlign: 'center',
            }}>
            {numberQuestionShow.current < 49 ? 'Tiếp tục' : 'Nộp bài'}
          </Text>
        </TouchableOpacity>
      </>
    );
  return <Spiner />;
}
