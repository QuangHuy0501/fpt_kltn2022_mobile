import {
  View,
  Text,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useRef, useState} from 'react';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import API from '../API/api';
import {
  currentStack,
  Current_Screen,
  Current_Screen_Params,
  Holland_Answers_Array,
  Holland_Questions_Array,
  Holland_Result_Array,
  LoginAppToken,
} from '../Constant-Storage';
import {ActivityIndicator} from 'react-native-paper';

export default function TestDraft01({navigation, route}) {
  const page = route.params.page;
  const [dataQuestion, setDataQuestion] = useState(
    route.params.dataHollandTest,
  );
  const [positionAnswer, setPositionAnswer] = useState([]);
  const scrollHolland = useRef();
  const [dataRender, setDataRender] = useState(dataQuestion);
  const [dataResult, setDataResult] = useState([]);
  const ScreenWidth = Dimensions.get('screen').width;
  const ScreenHeight = Dimensions.get('screen').height;
  const [loading, setLoading] = useState(false);
  const [LastPageInText, setLastPageInText] = useState('');

  useEffect(() => {
    setLoading(false);
    const star = [0, 0, 0, 0, 0];
    const temp = [];
    //lay cau tra loi cua tung cau hoi holland
    const getHollandAnswers = async item => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      console.log(tokenDnhap);
      const api = new API();
      api
        .onCallAPI(
          'get',
          `answers/question/${item.id}`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            // console.log(item);
            Object.assign(item, {answers: res.data});
            temp.push(item);
          }
        })
        .catch(err => alert(err));
    };
    ///add thêm properties star để render sao
    route.params.dataHollandTest.map((item, index) => {
      if (!item.hasOwnProperty('star')) {
        Object.assign(item, {star: star});
        getHollandAnswers(item);
      }
    });
    const savePageHoland = async () => {
      await AsyncStorage.setItem(currentStack, 'DoTest01');
      await AsyncStorage.setItem(Current_Screen_Params, page.toString());
      // await AsyncStorage.setItem(
      //   Holland_Answers_Array,
      //   JSON.stringify(dataResult),
      // );
    };
    savePageHoland();
    setDataQuestion(temp);
  }, [page]);

  const chooseStar = async (index, indexQuestion) => {
    const star = [0, 0, 0, 0, 0];
    const temp = [...star];
    const tempResult = [...dataRender];
    var i, j;
    for (i = 0; i <= index; i++) {
      temp[i] = 1;
    }
    tempResult[indexQuestion].star = temp;
    if (page === 1)
      await AsyncStorage.setItem(
        Holland_Answers_Array,
        JSON.stringify(tempResult),
      );
    else
      await AsyncStorage.setItem(
        Holland_Questions_Array,
        JSON.stringify(tempResult),
      );
    console.log(tempResult);
    setDataRender([...tempResult]);
    // setDataResult([...tempResult]);
  };
  // console.log(dataRender);
  const submitTest = async data => {
    const param = [];
    data.map(item => param.push(item.answerId));
    // return;
    // console.log(param);
    // return;
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    api
      .onCallAPI(
        'post',
        `answers/result/${2}`,
        {listAnswerId: param},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(async res => {
        if (res.data) {
          // console.log(res.data);
          await AsyncStorage.setItem(currentStack, 'personalityGrResult');
          const test = '2';
          await AsyncStorage.setItem(Current_Screen_Params, test);
          navigation.navigate('personalityGrResult', {
            testid: 2,
          });
        }
      })
      .catch(err => alert(err));
  };
  function compare(a, b) {
    if (a.indext < b.indext) {
      return -1;
    }
    if (a.indext > b.indext) {
      return 1;
    }
    return 0;
  }
  const NextBtnPress = async () => {
    setLoading(true);
    //code chỗ này check đã chọn sao đủ hết các câu trl chưa
    for (var i = 0; i < dataRender.length; i++) {
      if (dataRender[i].star.indexOf(0) === 0) {
        Alert.alert(
          'Thông báo',
          'Bạn cần chọn đủ các đáp án mới có thể sang trang',
          [
            {
              text: 'OK',
            },
          ],
        );

        //alert('Bạn cần chọn đủ các đáp án mới có thể sang trang');
        //position = {x: ..., y:...}
        const position = positionAnswer[i];
        //chỗ này là để nó auto scroll tới câu chưa chọn sao, ...
        //là để gộp các key của object position vào cái object dưới
        scrollHolland.current.scrollTo({...position, animated: false});
        setLoading(false);

        return;
      }
    }
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    api
      .onCallAPI(
        'get',
        'questions/holland',
        {},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(async res => {
        if (res.data) {
          // console.log(res.data);
          //data Result
          let temp;
          let resultFinal = [];
          let test = await AsyncStorage.getItem(Holland_Result_Array);
          console.log(test);
          // return;
          if (test?.length > 0) {
            const result = JSON.parse(test);
            temp = [...result];
            resultFinal = [...result];
          } else {
            temp = [...dataResult];
            test = [];
          }

          // console.log(temp, test);
          dataRender.map((item, i) => {
            const answerChoose = item.star.lastIndexOf(1);
            resultFinal.push(item.answers[answerChoose]);
            temp.push(item.answers[answerChoose]);
          });
          setDataResult(temp);
          console.log(resultFinal);
          // return;
          await AsyncStorage.setItem(
            Holland_Result_Array,
            JSON.stringify(resultFinal),
          );
          await AsyncStorage.setItem(
            Holland_Answers_Array,
            JSON.stringify(temp),
          );
          console.log(resultFinal.length);
          setLastPageInText(resultFinal);
          if(resultFinal.length >= 54){
            submitTest(temp);
            return;
          }
          const dataParams = res.data.slice(page * 10, (page + 1) * 10);
          console.log(dataParams);
          setDataRender(dataParams);
          await AsyncStorage.setItem(
            Holland_Questions_Array,
            JSON.stringify(dataParams),
          );
          await AsyncStorage.setItem(
            Current_Screen_Params,
            (page + 1).toString(),
          );
          // console.log(temp);
          // return;
          const hello = {
            a: await AsyncStorage.getItem(Current_Screen),
            b: await AsyncStorage.getItem(Current_Screen_Params),
            c: await AsyncStorage.getItem(Holland_Answers_Array),
            d: await AsyncStorage.getItem(Holland_Questions_Array),
            e: await AsyncStorage.getItem(currentStack),
          };
          console.log(hello);
          navigation.navigate('DoTest01', {
            dataHollandTest: dataParams,
            page: page + 1,
          });
          scrollHolland.current.scrollTo({x: 0, y: 0, animated: false});
        }
      })
      .catch(err => {
        alert(err);
        setLoading(false);
      });
  };

  const renderQuestion = (item, index) => {
    const temp = [...positionAnswer];
    return (
      <View
        key={index}
        //lưu vị trí các câu trả lời trên màn hình,
        //để khi ktr nếu chưa có thì auto scroll
        onLayout={e => {
          temp.push({x: e.nativeEvent.layout.x, y: e.nativeEvent.layout.y});
          setPositionAnswer(temp);
        }}
        style={{
          height: ScreenHeight * 0.25,
          backgroundColor: 'white',
          marginTop: 10,
          marginBottom: 15,
          borderRadius: 20,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          marginHorizontal: 20,
        }}>
        <View style={{flex: 1, justifyContent: 'center', marginHorizontal: 10}}>
          <Text style={{fontSize: 17, textAlign: 'left'}}>
            {item.indext}/54
          </Text>
        </View>
        <View
          style={{
            flex: 2,
            justifyContent: 'center',
            borderBottomWidth: 1,
            borderColor: '#ff7f50',
            marginHorizontal: 10,
          }}>
          <Text style={{fontSize: 20, textAlign: 'center'}}>
            {item.content}
          </Text>
        </View>
        <View
          style={{
            flex: 2,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          {item.star?.map((it, id) => {
            return (
              <TouchableOpacity key={id} onPress={() => chooseStar(id, index)}>
                <AntDesign
                  name={it ? 'star' : 'staro'}
                  size={25}
                  color={it ? 'orange' : 'black'}
                />
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  };
  // return <></>;
  return (
    <View style={{flex: 1, position: 'relative'}}>
      <Image
        source={require('../../assets/HollandDescription.png')}
        resizeMode="contain"
        style={{width: ScreenWidth, height: 250}}
      />
      <ScrollView ref={scrollHolland} style={{backgroundColor: 'white'}}>
        {dataRender.map(renderQuestion)}
        {!loading ? (
          <View style={{alignItems: 'center', paddingBottom: 15}}>
            <TouchableOpacity
              onPress={() => NextBtnPress()}
              style={{
                width: 250,
                height: 60,
                backgroundColor: '#F07122',
                borderRadius: 30,
                justifyContent: 'center',
              }}>
              <Text style={{color: 'white', fontSize: 25, textAlign: 'center'}}>
                {LastPageInText.length >= 50 ? 'Nộp bài' : 'Tiếp tục'}
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View
            style={{
              width: 250,
              height: 50,
              backgroundColor: '#F07122',
              borderRadius: 30,
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <ActivityIndicator size="large" color="black" />
          </View>
        )}
      </ScrollView>
    </View>
  );
}
