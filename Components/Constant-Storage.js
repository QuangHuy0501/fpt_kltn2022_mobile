export const resultQuestion = 'DataResult';
export const LOGOUT_USER = 'LOGOUT_USER';
export const LoginAppToken = 'LoginAppToken';
export const userEmail = 'userEmail';
export const avtURL = 'avtURL';
export const userFullName = 'userFullName';
export const testMBTI = 'testMBTI';
export const indexQuestionMBTI = 'indexQuestionMBTI';
export const UserIDApp = 'UserIDApp';
export const majorUserChoose = 'majorUserChoose';
export const firestoreUID = 'gMUUHUkDhYQZBJ6XgEn2ZYmFgmG3';
export const Current_Screen = 'Current_Screen';
export const Current_Screen_Params = 'Current_Screen_Params';
export const IntroShow = 'IntroIsShown';
export const questionID_MBTI = 'questionID_MBTI';
export const currentStack = 'currentStack';
export const Grade_Arrays_Screen = 'Grade_Arrays_Screen';

//array chứa answer đã chọn (trên 1 trang) (có chứa cả câu hỏi của trang đang trc khi out)
//nếu page đang trang tiếp theo, chưa chọn câu nào, thì answer sẽ chứa câu trả lời của trang trc
export const Holland_Answers_Array = 'Holland_Answers_Array';
//chứa câu hỏi 
export const Holland_Questions_Array = 'Holland_Questions_Array';
//array chứa toàn bộ câu trả lời dạng này ([{answerId: 161, answerContent: '1', orderIndex: 0}])
export const Holland_Result_Array = 'Holland_Result_Array';
