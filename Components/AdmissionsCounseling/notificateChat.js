import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  Dimensions,
  ScrollView,
  BackHandler,
} from 'react-native';
import React, {useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default function NotificateChat({navigation}) {
  const ScreenHeight = Dimensions.get('screen').height;
  const ScreenWidth = Dimensions.get('screen').width;

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  return (
    <View style={{flex: 1, position: 'relative'}}>
      <View
        style={{
          height: 70,
          justifyContent: 'center',
          borderRadius: 10,
          borderWidth: 1,
          borderColor: '#8758FF',
          marginHorizontal: 10,
          marginTop: 20,
          marginBottom: 40,
        }}>
        <View
          style={{
            paddingHorizontal: 20,
            flexDirection: 'row',
          }}>
          <View style={{flex: 1}}></View>
          <View style={{flex: 3, alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 32,
                color: 'black',
                textAlign: 'center',
                alignSelf: 'center',
              }}>
              Thông báo
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity
              onPress={() =>
                navigation.push('mainBoard', {
                  avtURL: '',
                  userName: '',
                })
              }
              style={{
                flex: 1,
                justifyContent: 'center',
                height: 40,
                width: 40,
                borderRadius: 20,
                backgroundColor: '#EEF1FF',
                alignItems: 'center',
              }}>
              <Ionicons name="close-outline" size={32} color="#400D51" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          height: ScreenHeight * 0.6,
          flex: 1,
          justifyContent: 'center',
        }}>
        <ScrollView>
          <View
            style={{
              height: 60,
              width: '95%',
              backgroundColor: 'white',
              borderRadius: 20,
              elevation: 50,
              shadowOffset: {width: 5, height: 5},
              shadowColor: 'black',
              marginVertical: 10,
              marginHorizontal: 5,
              alignItems: 'center',
              flexDirection: 'row',
              alignSelf: 'center',
            }}>
            <View
              style={{
                height: 40,
                width: 40,
                borderRadius: 20,
                backgroundColor: '#EEF1FF',
                justifyContent: 'center',
                marginLeft: 20,
                marginRight: 20,
                alignItems: 'center',
              }}>
              <MaterialIcons
                name="notifications-on"
                size={32}
                color="#400D51"
              />
            </View>
            <Text
              style={{
                color: 'black',
                fontSize: 22,
                textAlign: 'left',
              }}>
              Bạn có tin nhắn từ Tân
            </Text>
          </View>
          <View
            style={{
              height: 60,
              width: '95%',
              backgroundColor: 'white',
              borderRadius: 20,
              elevation: 50,
              shadowOffset: {width: 5, height: 5},
              shadowColor: 'black',
              marginVertical: 10,
              marginHorizontal: 5,
              alignItems: 'center',
              flexDirection: 'row',
              alignSelf: 'center',
            }}>
            <View
              style={{
                height: 40,
                width: 40,
                borderRadius: 20,
                backgroundColor: '#EEF1FF',
                justifyContent: 'center',
                marginLeft: 20,
                marginRight: 20,
                alignItems: 'center',
              }}>
              <MaterialIcons
                name="notifications-on"
                size={32}
                color="#400D51"
              />
            </View>
            <Text
              style={{
                color: 'black',
                fontSize: 22,
                textAlign: 'left',
              }}>
              Bạn có tin nhắn từ Danh
            </Text>
          </View>
          <View style={{height: 10}}></View>
        </ScrollView>
      </View>
    </View>
  );
}
