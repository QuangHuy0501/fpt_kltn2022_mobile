import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Modal,
  Pressable,
  StyleSheet,
  BackHandler,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {SafeAreaView} from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import API from '../API/api';
import {LoginAppToken} from '../Constant-Storage';
import Spiner from '../Loading/Spiner';

export default function Counseling({navigation, route}) {
  const [schools, setSchools] = useState([]);
  const [connectors, setConnectors] = useState([]);
  const [loading, isLoading] = useState(true);
  const [pageNumber, setPageNumber] = useState(1);
  const [modalVisible, setModalVisible] = useState(false);

  const pageSize = 10;
  const headerHeight = 190;

  const ScreenWidth = Dimensions.get('screen').width;
  const ScreenHeight = Dimensions.get('screen').height;
  useEffect(() => {
    console.log(route.params);
    if (route.params) {
      const {modalView, collegeId} = route.params;
      setModalVisible(modalView);
      getConnectors(collegeId);
    }
  }, [route.params]);

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  const getSchools = async (pageNumber, pageSize) => {
    isLoading(true);
    const bearerToken = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    await api
      .onCallAPI(
        'get',
        `colleges/Dashboard`,
        {},
        {PageNumber: pageNumber, PageSize: pageSize},
        {Authorization: 'bearer ' + bearerToken},
      )
      .then(res => {
        setSchools([...schools, ...res.data.college.data]);
      })
      .catch(err => {
        alert(err);
      })
      .finally(() => isLoading(false));
  };

  const getConnectors = async collegeId => {
    isLoading(true);
    const bearerToken = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    await api
      .onCallAPI(
        'get',
        `colleges/${collegeId}/connector`,
        {},
        {},
        {Authorization: 'bearer ' + bearerToken},
      )
      .then(res => {
        setConnectors([...res.data]);
        setModalVisible(true);
      })
      .catch(err => {
        alert(err);
      })
      .finally(() => isLoading(false));
  };

  useEffect(() => {
    getSchools(pageNumber, pageSize);
  }, [pageNumber]);

  const renderBtnSchools = ({item, index}) => (
    <TouchableOpacity
      style={{
        width: ScreenWidth * 0.9,
        height: 50,
        backgroundColor: index % 2 === 0 ? '#DAE5D0' : '#F9EBC8',
        justifyContent: 'center',
      }}
      onPress={() => {
        // navigation.navigate('Chat', {roomId: 1})
        getConnectors(item.collegeId);
      }}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 20,
          color: 'black',
        }}>{`${item.collegeId} : ${item.collegeName}`}</Text>
    </TouchableOpacity>
  );
  const renderBtnConnectors = ({item, index}) => (
    <TouchableOpacity
      style={{
        width: ScreenWidth,
        height: 50,
        backgroundColor: index % 2 === 0 ? '#DAE5D0' : '#F9EBC8',
        justifyContent: 'center',
      }}
      onPress={async () => {
        // navigation.navigate('Chat', {roomId: 1})
        // getConnectors(item.collegeId)
        const bearerToken = await AsyncStorage.getItem(LoginAppToken);
        // console.log(item.accountId, bearerToken);
        // return;

        await new API()
          .onCallAPI(
            'post',
            `sys_users/${item.accountId}/openchat`,
            {},
            {},
            {Authorization: 'bearer ' + bearerToken},
          )
          .then(res => {
            const roomId = res.data.id;
            navigation.navigate('Chat', {roomId: roomId});
          })
          .catch(err => {
            Alert.alert('Thông báo', 'Vui lòng đợi tư vấn', [
              {
                text: 'OK',
                onPress: () => setModalVisible(false),
              },
            ]);
          })
          .finally(() => isLoading(false));
        setModalVisible(false);
      }}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 20,
          color: 'black',
        }}>{`${item.accountId} : ${item.name}`}</Text>
    </TouchableOpacity>
  );

  if (loading) return <Spiner />;

  return (
    <View style={{flex: 1, position: 'relative'}}>
      <Modal
        animationType="slide"
        visible={modalVisible}
        // transparent={true}
      >
        <View style={styles.centeredView}>
          <Text
            style={{
              fontSize: 22,
              textDecorationLine: 'underline',
              marginBottom: 20,
            }}>
            Danh sách tư vấn viên
          </Text>
          <FlatList
            data={connectors}
            renderItem={renderBtnConnectors}
            keyExtractor={item => item.accountId}
          />
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => {
              if (route.params) {
                if (route.params?.modalView) navigation.goBack();
              } else setModalVisible(false);
            }}>
            <Text style={{color: 'white', fontSize: 20, textAlign: 'center'}}>
              Đóng
            </Text>
          </Pressable>
          <View style={{height: 10}}></View>
        </View>
      </Modal>
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          height: headerHeight,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          justifyContent: 'center',
        }}>
        <SafeAreaView style={{paddingBottom: 10}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 0.5, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{flex: 1}}>
                <Ionicons name="arrow-back-outline" size={32} color="white" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 32,
                color: 'white',
                textAlign: 'center',
                paddingTop: 30,
              }}>
              Trang tư vấn
            </Text>
            <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
          </View>
        </SafeAreaView>
      </LinearGradient>
      <SafeAreaView
        style={{
          height: ScreenHeight - headerHeight - 10 - 70, // height of Botttom Button + Margin
          marginHorizontal: 20,
          marginTop: 10,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <FlatList
          data={schools}
          renderItem={renderBtnSchools}
          keyExtractor={item => item.collegeId}
        />
        <TouchableOpacity
          style={{
            width: 150,
            height: 50,
            backgroundColor: '#FD841F',
            justifyContent: 'center',
            borderRadius: 10,
            marginTop: 10,
            marginBottom: 10,
          }}
          onPress={() => {
            getSchools(pageNumber + 1, pageSize);
            setPageNumber(pageNumber + 1);
          }}>
          <Text style={{textAlign: 'center', fontSize: 20, color: 'black'}}>
            Tải thêm
          </Text>
        </TouchableOpacity>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  button: {
    width: 200,
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    // justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
});
