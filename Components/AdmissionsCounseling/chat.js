import React, {useState, useEffect, useLayoutEffect} from 'react';
import {GiftedChat, Send} from 'react-native-gifted-chat';
import AsyncStorage from '@react-native-async-storage/async-storage';
import firestore from '@react-native-firebase/firestore';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImageCropPicker from 'react-native-image-crop-picker';
import LinearGradient from 'react-native-linear-gradient';
import {
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  Text,
} from 'react-native';
import BtnRound from './btnRound';
import {avtURL, userFullName, userEmail} from '../Constant-Storage';

const COLLECTION = 'chats';

const checkTimestamp = (a, b) => {
  if (a.data().createdAt > b.data().createdAt) {
    return -1;
  }
  if (a.data().createdAt < b.data().createdAt) {
    return 1;
  }
  return 0;
};

export default ({route, navigation}) => {
  const [messages, setMessages] = useState([]);
  // const [roomId, setRoomId] = useState(route.params.roomId)
  const [currentUser, setCurrentUser] = useState({});
  const roomId = route.params.roomId;

  const handleChatData = res => {
    let data = res.docs.sort((a, b) => checkTimestamp(a, b));
    setMessages(
      data.map(doc => ({
        _id: doc.id,
        createdAt: doc.data().createdAt.toDate(),
        text: doc.data().text,
        user: doc.data().user,
        image: doc.data().image,
      })),
    );
  };

  useEffect(() => {
    async function initVar() {
      setCurrentUser({
        avatar: await AsyncStorage.getItem(avtURL),
        name: await AsyncStorage.getItem(userFullName),
        _id: await AsyncStorage.getItem(userEmail),
      });
    }

    initVar();
  }, []);

  useLayoutEffect(() => {
    firestore()
      .collection(COLLECTION)
      .where('roomId', '==', roomId)
      // .orderBy('createdAt', 'desc')
      .get()
      .then(res => {
        handleChatData(res);
      });

    firestore()
      .collection(COLLECTION)
      .where('roomId', '==', roomId)
      .onSnapshot(
        querySnapshot => {
          handleChatData(querySnapshot);
        },
        e => console.error(e),
      );
  }, [navigation]);

  const onSend = messages => {
    // setMessages(previousMessages => GiftedChat.append(previousMessages, messages))

    const msgStore = {
      createdAt: messages[0].createdAt,
      text: messages[0].text,
      roomId: roomId,
      user: currentUser,
    };

    firestore()
      .collection(COLLECTION)
      .add(msgStore)
      .then(() => console.log('Message add'));
  };

  const onPressCamera = () => {
    ImageCropPicker.openPicker({
      includeBase64: true,
    }).then(image => {
      const imageBase64 = `data:${image.mime};base64,${image.data}`;
      const msgStore = {
        createdAt: new Date(),
        // text: messages[0].text,
        image: imageBase64,
        roomId: roomId,
        user: currentUser,
      };
      firestore()
        .collection(COLLECTION)
        .add(msgStore)
        .then(() => console.log('Message add'));
    });
  };

  return (
    <>
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          height: 80,
          borderBottomLeftRadius: 5,
          borderBottomRightRadius: 5,
          justifyContent: 'center',
        }}>
        <SafeAreaView style={{}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 0.5, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{flex: 1, alignItems: 'center'}}>
                <Ionicons name="arrow-back-outline" size={32} color="white" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 30,
                color: 'white',
                textAlign: 'center',
              }}>
              Chat
            </Text>
            <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
          </View>
        </SafeAreaView>
      </LinearGradient>
      <GiftedChat
        style={{paddingBottom: 10}}
        messages={messages}
        showAvatarForEveryMessage={true}
        placeholder={'Nhập để chat ...'}
        onSend={messages => onSend(messages)}
        renderSend={props => (
          <View
            style={{flexDirection: 'row', alignItems: 'center', height: 60}}>
            <BtnRound
              icon="camera"
              iconColor={'#7FBCD2'}
              size={40}
              style={{marginHorizontal: 5}}
              onPress={() => onPressCamera()}
            />
            <Send {...props}>
              <View style={styles.btnSend}>
                <Ionicons name="ios-send" size={20} color="#7FBCD2" />
              </View>
            </Send>
          </View>
        )}
        user={currentUser}
      />
    </>
  );
};

const styles = StyleSheet.create({
  btnSend: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    // backgroundColor: "7FBCD2",
    // ...getShadow(),
    borderRadius: 50,
  },
});
