import 'react-native-gesture-handler';
import React, {useState} from 'react';
import LoadPage_01 from '../Loading/LoadPage_01';
import LoadPage_02 from '../Loading/LoadPage_02';
import ChooseTestPage from '../Loading/ChooseTestPage';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  NavigationContainer,
  NavigationContainerRefContext,
} from '@react-navigation/native';
import {GoogleLogin} from '../Login/GoogleLogin';
import LoginPage from '../Login';
import HomePage from '../Home';
import EditProfile from '../Profile/Edit';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerContent from '../Home/DrawerContent';
import {LogBox} from 'react-native';
import Intro01 from '../Introduce/intro_01';
import Intro02 from '../Introduce/intro_02';
import Intro03 from '../Introduce/intro_03';
import ResultHistory from '../History/resultHistory';
import InputGrade from '../Login/inputGrade';
import InputGPA from '../Login/inputGPA';
import TestDraft01 from '../DoTest/Dtest01';
import UnitMBTI from '../DoTest/UnitMBTI';
import ActiveUser from '../Login/active';
import PersonalResult from '../Result/personalResult';
import MajorResult from '../Result/majorResult';
import ResultHistoryDetail from '../History/resultHistoryDetail';
import ChooseColleges from '../Colleges/ChooseColleges';
import Notificate from '../Home/noti';
import WishList from '../Colleges/WishList';
import AllColleges from '../AboutUs/about';
import Chat from '../AdmissionsCounseling/chat';
import Counseling from '../AdmissionsCounseling/counseling';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AnimTab1 from './test';
import HomeMainBoard from '../Home/homemain';
import BottomNavigate from '../Home/BottomNavigate';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import CollegesDetail from '../Colleges/collegesDetail';
import Blog from '../Home/blog';
import BlogDetail from '../Home/blogDetail';
import NotificateChat from '../AdmissionsCounseling/notificateChat';
import CommentBlog from '../Home/commentBlog';
import LessonDetail from '../Home/lessonDetail';

LogBox.ignoreLogs(['Reanimated 2']);
LogBox.ignoreLogs(['EventEmitter.removeListener']);
LogBox.ignoreAllLogs();
const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();
//nhung thang se co tab
const StackNavigate = ({navigation, route}) => {
  return (
    <Stack.Navigator initialRouteName="HomeScreen">
      <Stack.Screen
        options={{headerShown: false}}
        name="HomeScreen"
        component={HomeMainBoard}
        initialParams={route.params}
      />
    </Stack.Navigator>
  );
};
const DrawerObj = ({navigation, route}) => {
  //console.log(route.params);
  return (
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen
        // nhận params từ login gửi qua
        initialParams={route.params}
        name="DrawerController"
        options={{headerShown: false}}
        component={StackNavigate}
      />
    </Drawer.Navigator>
  );
};

{
  /*design tab chuyển trang ở dưới*/
}

function MyTabs({navigation, route}) {
  const [showTabBar, setshowTabBar] = useState(true);
  return (
    <Tab.Navigator
      backBehavior="initialRoute"
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
        tabBarStyle: {
          backgroundColor: '#AD40AF',
          display: showTabBar ? 'flex' : 'none',
        },
        tabBarHideOnKeyboard: true,
        tabBarActiveTintColor: 'white',
        tabBarInactiveTintColor: 'black',
      }}>
      <Tab.Screen
        name="Profile"
        component={EditProfile}
        listeners={({navigation, route}) => ({
          tabPress: e => {
            setshowTabBar(false);
          },
        })}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, size}) => (
            <FontAwesome name="user" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Test"
        listeners={({navigation, route}) => ({
          tabPress: e => {
            setshowTabBar(false);
          },
        })}
        component={LoadPage_01}
        options={{
          tabBarLabel: 'Test',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="clipboard-text-play-outline"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeMainBoard}
        // initialParams={route.params}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={ResultHistory}
        listeners={({navigation, route}) => ({
          tabPress: e => {
            setshowTabBar(false);
          },
        })}
        options={{
          tabBarLabel: 'History',
          tabBarIcon: ({color, size}) => (
            <FontAwesome name="history" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Colleges"
        component={AllColleges}
        listeners={({navigation, route}) => ({
          tabPress: e => {
            setshowTabBar(false);
          },
        })}
        options={{
          tabBarLabel: 'Colleges',
          tabBarIcon: ({color, size}) => (
            <FontAwesome5 name="school" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default function Routes() {
  // return (
  //   <NavigationContainer>
  //     <AnimTab1 />
  //   </NavigationContainer>
  // );

  //nhung thang khong co tab se o day

  return (
    <NavigationContainer>
      {/* LoginPage MainHome*/}
      <Stack.Navigator initialRouteName="Intro01">
        <Stack.Screen
          options={{headerShown: false}}
          name="Intro01"
          component={Intro01}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Intro02"
          component={Intro02}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Intro03"
          component={Intro03}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="LoginPage"
          component={LoginPage}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Activate"
          component={ActiveUser}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="MainHome"
          component={DrawerObj}
          // AnimTab1
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="mainBoard"
          component={MyTabs}
          // AnimTab1
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Load01"
          component={LoadPage_01}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Load02"
          component={LoadPage_02}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="ChooseTestPage"
          component={ChooseTestPage}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="UniversityScreen"
          component={ChooseColleges}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="EditProfile"
          component={EditProfile}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="AllColleges"
          component={AllColleges}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="ResultHistory"
          component={ResultHistory}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="ResultHistoryDetail"
          component={ResultHistoryDetail}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="DoTest01"
          component={TestDraft01}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="DoTest02"
          component={UnitMBTI}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="personalityGrResult"
          component={PersonalResult}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="MajorResult"
          component={MajorResult}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="InputGrade"
          component={InputGrade}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="InputGPA"
          component={InputGPA}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Notificate"
          component={Notificate}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="WishList"
          component={WishList}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Counseling"
          component={Counseling}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Chat"
          component={Chat}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="CollegesDetail"
          component={CollegesDetail}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Blog"
          component={Blog}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="BlogDetail"
          component={BlogDetail}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="NotificateChat"
          component={NotificateChat}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="CommentBlog"
          component={CommentBlog}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="LessonDetail"
          component={LessonDetail}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
