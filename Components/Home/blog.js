import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  Dimensions,
  ScrollView,
  Alert,
  BackHandler,
} from 'react-native';
import React, {useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import API from '../API/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken} from '../Constant-Storage';
import {useState} from 'react';

export default function Blog({navigation}) {
  const ScreenHeight = Dimensions.get('screen').height;
  const [listBlog, setListBlog] = useState([]);

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    const getAllBlog = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `blogs`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            setListBlog(res.data);
          }
        })
        .catch(err => {
          alert(err);
        });
    };
    getAllBlog();
  }, []);

  const renderBlog = () => {
    return listBlog.map((item, index) => {
      return (
        <TouchableOpacity
          key={index}
          onPress={() => navigation.push('BlogDetail', {blogID: item.id})}
          style={{
            height: 200,
            width: '95%',
            backgroundColor: 'white',
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            borderRadius: 20,
            marginVertical: 15,
            flexDirection: 'row',
          }}>
          <View style={{flex: 1}}>
            <Image
              source={{
                uri: item.image
                  ? item.image
                  : 'https://i.chungta.vn/2020/02/13/fpt2020-1581568294.jpg',
              }}
              style={{
                width: '100%',
                height: '100%',
                borderTopLeftRadius: 20,
                borderBottomLeftRadius: 20,
              }}
            />
          </View>
          <View
            style={{flex: 2, borderTopLeftRadius: 70, paddingHorizontal: 10}}>
            <Text style={{color: 'black', fontSize: 20}}>{item.title}</Text>
            <Text style={{color: 'black', fontSize: 20, flexWrap: 'wrap'}}>
              {item.description.slice(0, 50)}...
              <Text style={{color: 'blue', fontSize: 20}}>Xem thêm.</Text>
            </Text>
            <View
              style={{
                flexDirection: 'row',
                height: 30,
                width: 100,
                marginTop: 10,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingRight: 20,
                }}>
                <AntDesign name="like1" size={30} color="#7FBCD2" />
                <Text style={{color: '#7FBCD2', fontSize: 20, paddingLeft: 5}}>
                  {item.numOfReact}
                </Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <MaterialIcons name="mode-comment" size={30} color="#7FBCD2" />
                <Text style={{color: '#7FBCD2', fontSize: 20, paddingLeft: 5}}>
                  {item.numOfComment}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      );
    });
  };
  return (
    <View style={{flex: 1, position: 'relative'}}>
      <ImageBackground
        source={require('../../assets/blogBackGr.jpg')}
        resizeMode="stretch"
        style={{
          flex: 1,
          borderBottomLeftRadius: 50,
          borderBottomRightRadius: 50,
          overflow: 'hidden',
        }}>
        <View
          style={{
            paddingHorizontal: 20,
            flexDirection: 'row',
          }}>
          <View style={{flex: 1, alignItems: 'flex-start'}}>
            <TouchableOpacity
              onPress={() =>
                navigation.push('mainBoard', {
                  avtURL: '',
                  userName: '',
                })
              }
              style={{flex: 1, justifyContent: 'center'}}>
              <Ionicons name="arrow-back-outline" size={35} color="white" />
            </TouchableOpacity>
          </View>
          <Text
            style={{
              flex: 3,
              fontSize: 32,
              color: 'white',
              textAlign: 'center',
              paddingTop: 30,
            }}>
            Personal Strengthen Blog
          </Text>
          <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
        </View>
      </ImageBackground>
      <View
        style={{
          width: '100%',
          height: ScreenHeight * 0.65,
          backgroundColor: 'white',
          borderTopLeftRadius: 50,
          borderTopRightRadius: 50,
          marginTop: 30,
          elevation: 50,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
        }}>
        <View style={{height: 40}}></View>
        <ScrollView
          style={{marginHorizontal: 5, backgroundColor: 'white'}}
          contentContainerStyle={{alignItems: 'center'}}>
          {renderBlog()}
          <TouchableOpacity
            onPress={() => ''}
            style={{
              height: 50,
              width: 150,
              backgroundColor: '#F07122D4',
              borderRadius: 20,
              elevation: 10,
              shadowOffset: {width: 5, height: 5},
              shadowColor: 'black',
              shadowOpacity: 1,
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <Text
              style={{
                fontSize: 22,
                color: 'white',
                textAlign: 'center',
              }}>
              Xem thêm
            </Text>
          </TouchableOpacity>
          <View style={{height: 10}}></View>
        </ScrollView>
        <View style={{height: 10}}></View>
      </View>
    </View>
  );
}
