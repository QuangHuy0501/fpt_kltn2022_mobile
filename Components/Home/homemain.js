import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  BackHandler,
} from 'react-native';
import React, {useState, useEffect} from 'react';

import Entypo from 'react-native-vector-icons/Entypo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {SafeAreaView} from 'react-native-safe-area-context';
import Modal from 'react-native-modal';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import API from '../API/api';
import {LoginAppToken} from '../Constant-Storage';
import RenderLesson from './RenderLesson';
import RenderHotCollege from './RenderHotCollege';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import Spiner from '../Loading/Spiner';
import Draggable from 'react-native-draggable';
import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';

export default function HomeMainBoard({navigation}) {
  const screenSize = Dimensions.get('window');
  const [visible, setVisible] = useState(false);
  const [avtToDisplay, setAvtToDisplay] = useState('');
  const [listNoti, setListNoti] = useState(null);
  const [listCollege, setlistCollege] = useState(null);
  const [displayName, setdisplayName] = useState('');
  const [displayPlus, setdisplayPlus] = useState(false);
  const [coordinate, setCoordinate] = useState({
    x: screenSize.width - 70,
    y: screenSize.height * 0.5,
  });

  useEffect(() => {
    const backAction = async () => {
      try {
        BackHandler.exitApp();
      } catch (err) {
        console.log(err);
      }
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const signOut = async () => {
    //console.log('dang xuat');
    try {
      GoogleSignin.configure({
        webClientId:
          'firebase-adminsdk-iyo41@personalstrength-83af0.iam.gserviceaccount.com',
      });
      // const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      // setloggedIn(false);
      // setuserInfo([]);
      await AsyncStorage.clear();
      navigation.navigate('LoginPage');
    } catch (err) {
      alert('loi log out' + err);
      await AsyncStorage.clear();
      navigation.navigate('LoginPage');
    }
  };

  useEffect(() => {
    return () => {
      setVisible(false);
    };
  }, []);

  useEffect(() => {
    async function test() {
      const token = await AsyncStorage.getItem(LoginAppToken);
      console.log(token);
      const api = new API();
      api
        .onCallAPI(
          'get',
          'sys_users/profile',
          {},
          {},
          {Authorization: 'bearer ' + token},
        )
        .then(res => {
          if (res) {
            const avtLink = res.data.imagePath;
            const userTemp = res.data.fullName;
            const displayTemp = userTemp.slice(
              userTemp.lastIndexOf(' '),
              userTemp.length,
            );
            setAvtToDisplay(avtLink);
            setdisplayName(displayTemp);
          }
        })
        .catch(err => console.log(err));
    }
    test();
  });
  useEffect(() => {
    const getAllNoti = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      api
        .onCallAPI(
          'get',
          `sys_users/lesson`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            setListNoti(res.data);
          }
        })
        .catch(err => alert(err));
      api
        .onCallAPI(
          'get',
          'colleges/SuggestedSum',
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            setlistCollege(res.data);
          }
        })
        .catch(err => console.log(err));
    };

    getAllNoti();
  }, []);
  // console.log(displayName, listCollege, listNoti);
  if (displayName && listCollege?.length >= 0 && listNoti?.length >= 0)
    // console.log('1');
    return (
      <>
        <View style={{flex: 3}}>
          <SafeAreaView style={{paddingBottom: 15, paddingTop: 20}}>
            <View
              style={{
                paddingHorizontal: 15,
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <View style={{flex: 1, alignItems: 'flex-start'}}>
                <TouchableOpacity
                  onPress={() => {
                    setdisplayPlus(!displayPlus);
                  }}
                  style={{flex: 1}}>
                  <Feather name="align-left" size={35} color="orange" />
                </TouchableOpacity>
                {displayPlus && (
                  <View
                    style={{
                      width: 200,
                      backgroundColor: 'white',
                      elevation: 50,
                      shadowOffset: {width: 5, height: 5},
                      shadowColor: 'black',
                      position: 'absolute',
                      left: 35,
                      bottom: -10,
                      borderRadius: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                      zIndex: 999,
                    }}>
                    <TouchableOpacity
                      onPress={() => navigation.navigate('WishList')}
                      style={{flex: 1, paddingBottom: 10}}>
                      <Text style={{color: 'black', fontSize: 20}}>
                        Nguyện vọng
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={signOut} style={{flex: 1}}>
                      <Text style={{color: 'black', fontSize: 20}}>
                        Đăng xuất
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
              <View
                style={{
                  justifyContent: 'flex-end',
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Blog')}
                  style={{paddingRight: 20}}>
                  <Fontisto name="blogger" size={35} color="orange" />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => navigation.navigate('NotificateChat')}
                  style={{}}>
                  <Entypo name="bell" size={35} color="orange" />
                </TouchableOpacity>
                <Image
                  style={{
                    width: 70,
                    height: 70,
                    borderRadius: 35,
                    marginLeft: 15,
                  }}
                  source={{
                    uri: avtToDisplay
                      ? avtToDisplay
                      : 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
                  }}
                />
                {/* thiết kế cho chuông thông báo */}
              </View>
              {/*view avartar*/}

              {/* {avtToDisplay.length > 0 ? (
            <Image
              source={{
                uri: avtToDisplay
                  ? avtToDisplay
                  : 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
              }}
              resizeMode="cover"
              style={{width: '100%', height: '100%', borderRadius: 40}}
            />
          ) : (
            <></>
          )} */}
            </View>
            <View style={{paddingLeft: 15, paddingTop: 10}}>
              {/*view name*/}
              <View style={{justifyContent: 'flex-start'}}>
                <Text
                  style={{
                    textAlign: 'left',
                    color: 'black',
                    fontSize: 25,
                    fontWeight: 'bold',
                    fontFamily: 'Roboto',
                    fontFamily: 'sans-serif',
                  }}>
                  Xin chào {displayName}
                </Text>
                <Text
                  style={{
                    textAlign: 'left',
                    color: 'black',
                    fontFamily: 'sans-serif',
                    fontSize: 20,
                  }}>
                  Hãy cùng chúng tôi hiểu hơn về khả năng của bạn
                </Text>
              </View>
            </View>
          </SafeAreaView>
          <View style={{paddingHorizontal: 5}}>
            {/*design ds trường đề cử*/}
            <View>
              <Text
                style={{
                  textAlign: 'left',
                  color: 'black',
                  fontSize: 22,
                  fontWeight: 'bold',
                  paddingBottom: 10,
                  paddingLeft: 10,
                }}>
                Những trường đang hot
              </Text>
            </View>
            <View>
              <ScrollView horizontal style={{flexDirection: 'row'}}>
                <RenderHotCollege data={listCollege} />
              </ScrollView>
              {/*design ds khóa học đề cử*/}
              <View>
                <Text
                  style={{
                    textAlign: 'left',
                    color: 'black',
                    fontSize: 22,
                    fontWeight: 'bold',
                    paddingBottom: 10,
                    paddingTop: 10,
                    paddingLeft: 10,
                  }}>
                  Các khóa học hấp dẫn
                </Text>
              </View>
              <ScrollView horizontal>
                <RenderLesson data={listNoti} />
              </ScrollView>
              {/*design bottom bar*/}
            </View>
            {/*design header */}
          </View>

          {/* <TouchableOpacity
          // onPress={navigation.navigate('Counseling')}
          style={{
            height: 70,
            width: 70,
            borderRadius: 35,
            backgroundColor: 'white',
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            position: 'absolute',
            right: 10,
            top: 320,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Entypo name="chat" size={45} color="orange" />
        </TouchableOpacity> */}
        </View>
        <Draggable
          x={coordinate.x} //{Dimensions.get('window').width - widthButton} //
          y={coordinate.y} //{Dimensions.get('window').height - 100}
          minX={0}
          minY={0}
          maxX={screenSize.width}
          maxY={screenSize.height + 150}
          renderColor="white"
          shadowColor="blue"
          isCircle
          // onDragRelease={(e, f, c) => setCoordinate({x: c.left, y: c.top})}
          onDrag={() => {}}
          onShortPressRelease={() => navigation.navigate('Counseling')}>
          <Entypo name="chat" size={60} color="orange" />
        </Draggable>
      </>
    );
  return <Spiner />;
}
