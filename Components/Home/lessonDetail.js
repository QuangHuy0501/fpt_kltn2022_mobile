import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Linking,
  BackHandler,
} from 'react-native';
import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import {useEffect} from 'react';
import {useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken} from '../Constant-Storage';
import API from '../API/api';

export default function LessonDetail({navigation, route}) {
  const {dataDetail} = route.params;
  console.log(dataDetail);
  const handlePress = async supportedURL => {
    const supported = await Linking.canOpenURL(supportedURL);
    if (supported) {
      await Linking.openURL(supportedURL);
    } else {
      Alert.alert(`Don't know how to open this URL: ${supportedURL}`);
    }
  };

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  return (
    <View style={{flex: 1, position: 'relative'}}>
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{
          height: 40,
          width: 40,
          backgroundColor: 'white',
          elevation: 50,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          position: 'absolute',
          left: 0,
          top: 0,
          justifyContent: 'center',
          alignItems: 'center',
          zIndex: 999,
        }}>
        <Feather name="arrow-left" size={35} color="orange" />
      </TouchableOpacity>
      <View>
        <Image
          style={{width: '100%', height: 250}}
          source={
            dataDetail.imageUrl.length > 0
              ? {uri: dataDetail.imageUrl}
              : {
                  uri: 'https://i.chungta.vn/2020/02/13/fpt2020-1581568294.jpg',
                }
          }
        />
      </View>
      <View style={{paddingHorizontal: 10, paddingTop: 20}}>
        <View>
          <Text
            style={{
              textAlign: 'left',
              color: 'black',
              fontSize: 25,
              fontWeight: 'bold',
            }}>
            Khoa hoc react
          </Text>
          <View style={{height: 200, marginTop: 20}}>
            <ScrollView style={{flex: 1}}>
              <Text
                style={{
                  textAlign: 'left',
                  color: 'black',
                  fontSize: 20,
                  paddingTop: 15,
                }}>
                {dataDetail.description}
              </Text>
            </ScrollView>
          </View>
        </View>
      </View>
      <View
        style={{
          justifyContent: 'center',
          alignSelf: 'center',
          height: 70,
          width: 250,
          marginTop: 20,
          marginLeft: 15,
          borderRadius: 45,
          elevation: 50,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          backgroundColor: '#ABD9FF',
        }}>
        <TouchableOpacity
          onPress={() => handlePress(dataDetail.link)}
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: 'black',
              fontSize: 22,
              textAlign: 'center',
              padding: 5,
              fontFamily: 'Tahoma',
            }}>
            Tham khảo website
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
