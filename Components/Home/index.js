import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  Linking,
  BackHandler,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import Entypo from 'react-native-vector-icons/Entypo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LoginAppToken,
  avtURL,
  userFullName,
  Current_Screen,
  Current_Screen_Params,
} from '../Constant-Storage';
import {SafeAreaView} from 'react-native-safe-area-context';
import Modal from 'react-native-modal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import API from '../API/api';
import { GoogleSignin } from '@react-native-google-signin/google-signin';

export default function HomePage({navigation, route}) {
  const [nameToDisplay, setnameToDisplay] = useState('');
  const {avtURL, userName} = route.params;
  const [avtToDisplay, setAvtToDisplay] = useState('');
  const [visible, setVisible] = useState(false);
  const [displayName, setdisplayName] = useState('');
  const [listNoti, setListNoti] = useState([]);

  useEffect(() => {
    const backAction = async () => {
      try {
        BackHandler.exitApp();
      } catch (err) {
        console.log(err);
      }
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []); 

  useEffect(() => {
    const SaveCurrentScreen = async () => {
      await AsyncStorage.setItem(Current_Screen, 'HomeScreen');
      // await AsyncStorage.setItem(Current_Screen_Params, '');
    };
    SaveCurrentScreen();
  }, []);

  useEffect(() => {
    return () => {
      setVisible(false);
    };
  }, []);

  useEffect(() => {
    async function test() {
      const token = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      api
        .onCallAPI(
          'get',
          'sys_users/profile',
          {},
          {},
          {Authorization: 'bearer ' + token},
        )
        .then(res => {
          if (res) {
            const avtLink = res.data.imagePath;
            const userTemp = res.data.fullName;
            const displayTemp = userTemp.slice(
              userTemp.lastIndexOf(' '),
              userTemp.length,
            );
            setAvtToDisplay(avtLink);
            setnameToDisplay(userTemp);
            setdisplayName(displayTemp);
          }
        })
        .catch(err => console.log(err));
    }
    test();
  });
  const openNotification = async () => {
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    console.log(tokenDnhap);
    return;
    const api = new API();
    api
      .onCallAPI(
        'get',
        `sys_users/lesson_by_major`,
        {},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(res => {
        if (res.data) {
          console.log(res.data);
          setListNoti(res.data);
          setVisible(true);
        }
      })
      .catch(err => {
        alert(err);
        setVisible(true);
      });
  };
  const openLink = async supportedURL => {
    const supported = await Linking.canOpenURL(supportedURL);
    if (supported) {
      await Linking.openURL(supportedURL);
    } else {
      // await Linking.openURL(supportedURL);
      Alert.alert(`Don't know how to open this URL: ${supportedURL}`);
    }
  };
  // useEffect(() => {
  //   const getNotificate = async () => {
  //     const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
  //     const api = new API();
  //     await api
  //       .onCallAPI(
  //         'get',
  //         `sys_users/lession`,
  //         {},
  //         {},
  //         {Authorization: 'bearer ' + tokenDnhap},
  //       )
  //       .then(res => {
  //         if (res.data) {
  //           console.log(res.data);
  //           setListNoti(res.data);
  //         }
  //       })
  //       .catch(err => alert(err));
  //   };
  //   getNotificate();
  // }, []);

  return (
    <View style={{flex: 3, position: 'relative'}}>
      {/*design header */}
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          height: 190,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          justifyContent: 'center',
          //textAlign: 'center'
        }}>
        <SafeAreaView style={{paddingBottom: 15}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 1, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() => {
                  navigation.toggleDrawer();
                }}
                style={{flex: 1}}>
                <Entypo name="menu" size={32} color="white" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 34,
                color: 'white',
                textAlign: 'center',
                paddingTop: 30,
              }}>
              HOME
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}>
              <TouchableOpacity onPress={openNotification} style={{flex: 1}}>
                <Entypo name="bell" size={32} color="white" />
              </TouchableOpacity>
              {/* thiết kế cho modal */}
              <Modal
                isVisible={visible}
                animationIn="zoomInDown"
                onBackdropPress={() => setVisible(false)}
                animationOut="zoomOutUp"
                backdropOpacity={0}
                animationOutTiming={600}
                propagateSwipe={false}>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: 'white',
                    borderRadius: 20,
                    elevation: 20,
                    shadowOffset: {width: 10, height: 10},
                    shadowColor: 'black',
                    shadowOpacity: 1,
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      marginVertical: 10,
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        flex: 1,
                        fontSize: 30,
                        color: '#F07122',
                        textAlign: 'center',
                        alignSelf: 'center',
                      }}>
                      Thông báo
                    </Text>
                    <TouchableOpacity
                      onPress={() => setVisible(false)}
                      style={{flex: 1, position: 'absolute', right: 10}}>
                      <AntDesign name="close" size={35} color="black" />
                    </TouchableOpacity>
                  </View>
                  <View style={{flex: 3}}>
                    <ScrollView style={{}}>
                      {listNoti.map((item, index) => {
                        return (
                          <View
                            key={index}
                            style={{
                              flex: 1,
                              flexDirection: 'row',
                              width: 300,
                              backgroundColor: 'white',
                              borderRadius: 20,
                              elevation: 20,
                              shadowOffset: {width: 10, height: 10},
                              shadowColor: 'black',
                              shadowOpacity: 1,
                              marginVertical: 10,
                              marginHorizontal: 5,
                            }}>
                            <View
                              style={{
                                flex: 1,
                                marginHorizontal: 10,
                                justifyContent: 'center',
                              }}>
                              <Image
                                source={
                                  item.imageUrl
                                    ? {uri: item.imageUrl}
                                    : require('../../assets/NotificationLogo.jpg')
                                }
                                resizeMode="cover"
                                style={{
                                  width: 80,
                                  height: 80,
                                  borderRadius: 40,
                                  alignSelf: 'center',
                                }}
                              />
                            </View>
                            <View style={{flex: 3, marginVertical: 20}}>
                              <Text style={{fontSize: 18}}>
                                {item.description}
                              </Text>
                              <TouchableOpacity
                                onPress={() => openLink(item.link)}>
                                <Text style={{fontSize: 18}}>
                                  Nhấn vào đây!!!
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        );
                      })}
                    </ScrollView>
                  </View>
                  <View
                    style={{
                      alignItems: 'center',
                      flex: 1,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        navigation.push('Notificate');
                        setVisible(false);
                      }}
                      style={{
                        width: 250,
                        height: 50,
                        backgroundColor: '#F07122',
                        marginVertical: 30,
                        borderRadius: 30,
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{
                          color: 'white',
                          fontSize: 24,
                          textAlign: 'center',
                        }}>
                        Xem tất cả khóa học
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
            </View>
          </View>
        </SafeAreaView>
      </LinearGradient>
      {/*design avatar và nút làm test */}
      <View style={{flexDirection: 'row', flex: 1, paddingTop: 15}}>
        <View style={{width: 80, height: 80, marginLeft: 10}}>
          {avtToDisplay.length > 0 ? (
            <Image
              source={{
                uri: avtToDisplay
                  ? avtToDisplay
                  : 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
              }}
              resizeMode="cover"
              style={{width: '100%', height: '100%', borderRadius: 40}}
            />
          ) : (
            <></>
          )}
        </View>
        <View
          style={{
            flexDirection: 'column',
            paddingLeft: 20,
          }}>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: '#332600',
            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>
              Xin chào, {displayName}
            </Text>
          </View>
          <View style={{}}>
            <Text style={{fontSize: 18}}>Cùng chúng tôi hiểu hơn</Text>

            <View style={{flexDirection: 'row'}}>
              <Text style={{fontSize: 18}}>về khả năng của bạn</Text>
            </View>
          </View>
        </View>
        <View style={{flex: 0.1}}></View>
      </View>
      {/*design phần thân trang*/}
      <View
        style={{flexDirection: 'row', flex: 3, backgroundColor: 'transparent'}}>
        <View
          style={{
            flex: 1,
            borderRightWidth: 1,
            borderRightColor: 'red',
          }}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              paddingTop: 50,
            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Kiểm tra</Text>
            <TouchableOpacity
              style={{}}
              onPress={() => navigation.push('Load01')}>
              <Image
                source={require('../../assets/HomeRetakeTest.jpg')}
                resizeMode="contain"
                style={{width: 100, height: 100}}
              />
            </TouchableOpacity>
            <Text style={{fontSize: 18, textAlign: 'center'}}>
              Làm bài kiểm tra hoặc làm lại
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
          }}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              paddingTop: 50,
            }}>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>
              Danh sách trường
            </Text>
            <TouchableOpacity
              style={{}}
              onPress={() => navigation.push('AllColleges')}>
              {/* UniversityScreen */}
              <Image
                source={require('../../assets/dsSchoolIcon.jpg')}
                resizeMode="contain"
                style={{width: 100, height: 100}}
              />
            </TouchableOpacity>
            <Text style={{fontSize: 18, textAlign: 'center'}}>
              Xem trường và điểm tuyển sinh
            </Text>
          </View>
        </View>
      </View>
      {/*design bottom bar*/}
      <View
        style={{
          width: 80,
          height: 30,
          borderRadius: 40,
          flex: 1,
          alignSelf: 'center',
        }}></View>
    </View>
  );
}
