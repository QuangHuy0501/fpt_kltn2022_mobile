import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image,
  Linking,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import API from '../API/api';
import {LoginAppToken} from '../Constant-Storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spiner from '../Loading/Spiner';
import {Searchbar} from 'react-native-paper';

export default function Notificate({navigation}) {
  const ScreenHeight = Dimensions.get('screen').height;
  const Screenwidth = Dimensions.get('screen').width;
  const [searchQuery, setSearchQuery] = React.useState('');
  const [listSearch, setListSearch] = useState([]);
  const onChangeSearch = query => {
    setSearchQuery(query);
    const tempNoti = [...listNoti];
    // console.log();
    // console.log(temp[0].name.includes(test));
    // return;
    const listSearch = [];
    tempNoti.map((item, index) => {
      const majorSearch = item.major.name.trim();
      const test = removeVietnameseTones(majorSearch)
        .toLowerCase()
        .includes(removeVietnameseTones(query).toLowerCase());
      if (test) listSearch.push(item);
    });
    setListSearch(listSearch);
    console.log(listSearch);
  };
  const [visible, setVisible] = useState(false);
  const [listNoti, setListNoti] = useState();
  function removeVietnameseTones(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
    str = str.replace(/Đ/g, 'D');
    // Some system encode vietnamese combining accent as individual utf-8 characters
    // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ''); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    // Remove extra spaces
    // Bỏ các khoảng trắng liền nhau
    str = str.replace(/ + /g, ' ');
    str = str.trim();
    // Remove punctuations
    // Bỏ dấu câu, kí tự đặc biệt
    str = str.replace(
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
      ' ',
    );
    return str;
  }
  useEffect(() => {
    const getAllNoti = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `sys_users/lesson`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            setListNoti(res.data);
          }
        })
        .catch(err => alert(err));
    };
    getAllNoti();
  }, []);
  const renderNoti = (item, index) => {
    const supportedURL = item.link;
    return (
      <View
        key={index}
        style={{
          backgroundColor: 'white',
          height: ScreenHeight * 0.25,
          marginVertical: 5,
          borderRadius: 20,
          elevation: 10,
          shadowOffset: {width: 10, height: 10},
          shadowColor: 'black',
          shadowOpacity: 1,
          marginHorizontal: 5,
          justifyContent: 'center',
          marginBottom: 20,
        }}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Image
              source={{
                uri: item.imageUrl,
              }}
              resizeMode="cover"
              style={{
                height: 80,
                width: 80,
                borderRadius: 40,
                marginLeft: 10,
              }}
            />
          </View>
          <View style={{flex: 2, marginRight: 10}}>
            <Text style={{fontSize: 20}}> Khóa học: {item.major.name}</Text>
            <Text style={{fontSize: 20}}> {item.description}</Text>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <TouchableOpacity onPress={() => handlePress(supportedURL)}>
                <Text style={{fontSize: 20, color: 'orange'}}>
                  {' '}
                  Chi tiết nhấn vào đây
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };
  const notiRender = () => {
    const handlePress = async supportedURL => {
      const supported = await Linking.canOpenURL(supportedURL);
      if (supported) {
        await Linking.openURL(supportedURL);
      } else {
        Alert.alert(`Don't know how to open this URL: ${supportedURL}`);
      }
    };
    if (listSearch.length > 0) return listSearch.map(renderNoti);
    if (listNoti && listSearch.length === 0 && searchQuery.length === 0) {
      return listNoti.map(renderNoti);
    }
  };
  if (!listNoti) return <Spiner />;
  return (
    <View style={{flex: 1, position: 'relative'}}>
      {/*Design header*/}
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          height: 190,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          justifyContent: 'center',
        }}>
        <SafeAreaView style={{paddingBottom: 10}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 0.5, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{flex: 1}}>
                <Ionicons name="arrow-back-outline" size={32} color="white" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 32,
                color: 'white',
                textAlign: 'center',
                paddingTop: 30,
              }}>
              DANH SÁCH KHÓA HỌC
            </Text>
            <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
          </View>
        </SafeAreaView>
        <View
          style={{
            width: '100%',
            paddingTop: 10,
            paddingHorizontal: 30,
          }}>
          <Searchbar
            style={{borderRadius: 40, fontSize: 20}}
            placeholder="Tìm kiếm"
            onChangeText={onChangeSearch}
            value={searchQuery}
          />
        </View>
      </LinearGradient>
      {/*Design body*/}
      <View
        style={{
          flex: 1,
          borderTopLeftRadius: 50,
          borderTopRightRadius: 50,
          marginTop: 20,
          backgroundColor: '#FF8A80',
          paddingTop: 50,
          paddingHorizontal: 10,
        }}>
        <ScrollView
          style={{
            flex: 1,
            backgroundColor: 'white',
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            elevation: 20,
            shadowOffset: {width: 10, height: 10},
            shadowColor: 'black',
            shadowOpacity: 1,
            paddingHorizontal: 15,
            paddingVertical: 10,
          }}>
          {notiRender(listNoti)}
        </ScrollView>
      </View>
    </View>
  );
}
