import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  Dimensions,
  ScrollView,
  BackHandler,
  StyleSheet,
  Alert,
} from 'react-native';
import React, {useEffect} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken} from '../Constant-Storage';
import API from '../API/api';
import {useState} from 'react';
import {TextInput} from 'react-native-gesture-handler';

export default function CommentBlog({navigation, route}) {
  const {blogID} = route.params;
  // const blogID = 6;
  const [comments, setComments] = useState([]);
  const ScreenHeight = Dimensions.get('screen').height;
  const [messages, setMessages] = useState([]);

  const [input, setInput] = useState('');

  useEffect(() => {
    const detailBlog = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `blogs/${blogID}/comments`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            console.log(res.data);
            setComments(res.data);
          }
        })
        .catch(err => {
          alert(err);
        });
    };

    detailBlog();
  }, []);
  const renderCommentBlog = (item, index) => {
    return (
      <View
        key={index}
        style={{
          height: 60,
          backgroundColor: 'white',
          borderRadius: 20,
          elevation: 50,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          marginVertical: 10,
          marginHorizontal: 5,
          justifyContent: 'center',
        }}>
        <Text
          style={{
            color: 'black',
            fontSize: 22,
            textAlign: 'left',
            paddingLeft: 20,
          }}>
          {item.content}
        </Text>
      </View>
    );
  };
  const onSend = async text => {
    if (text.length === 0) {
      Alert.alert('bạn phải nhập nội dung comment');
      return;
    }
    const temp = [...comments];
    temp.push({
      content: input,
    });
    console.log(text, 'onsend');
    setComments(temp);
    console.log(temp);
    await onSubmitComment(text);
  };
  const onSubmitComment = async text => {
    console.log(text, 'onsubmit');
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    await api
      .onCallAPI(
        'post',
        `blogs/${blogID}/comment`,
        {content: text},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(res => {
        if (res.data) {
          console.log(res.data);
          setInput('');
          // setComments(res.data);
        }
      })
      .catch(err => {
        alert(err);
        setInput('');
      });
  };
  return (
    <View style={{flex: 1, position: 'relative'}}>
      <View
        style={{
          height: 70,
          justifyContent: 'center',
          borderRadius: 10,
          borderWidth: 1,
          borderColor: '#8758FF',
          marginHorizontal: 10,
          marginTop: 20,
          marginBottom: 40,
        }}>
        <View
          style={{
            paddingHorizontal: 20,
            flexDirection: 'row',
          }}>
          <View style={{flex: 1}}></View>
          <View style={{flex: 3, alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 32,
                color: 'black',
                textAlign: 'center',
                alignSelf: 'center',
              }}>
              Diễn đàn
            </Text>
          </View>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{
                flex: 1,
                justifyContent: 'center',
                height: 40,
                width: 40,
                borderRadius: 20,
                backgroundColor: '#EEF1FF',
                alignItems: 'center',
              }}>
              <Ionicons name="close-outline" size={32} color="#400D51" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          height: ScreenHeight * 0.6,
          flex: 1,
        }}>
        <ScrollView
          style={{
            paddingVertical: 20,
          }}>
          {comments.map(renderCommentBlog)}
          <View style={{height: 10}}></View>
        </ScrollView>
      </View>

      <View
        style={{
          flexDirection: 'row',
          borderWidth: 0.5,
          borderRadius: 20,
          marginHorizontal: 5,
          borderColor: 'rgba(0,0,0,.5)',
        }}>
        <TextInput
          multiline
          onChangeText={e => {
            setInput(e);
          }}
          placeholder="Nhập Bình luận..."
          // style={[styles.stylesViewLCS.textCmt, {maxHeight: 100}]}
          value={input}
          style={{
            flex: 10,
            fontSize: 16,
            paddingLeft: 10,
            maxHeight: 100,
          }}
        />
        {/* <View style={{width: 36, height: 36}} /> */}
        <TouchableOpacity
          onPress={() => {
            const content = input;
            onSend(content);
          }}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 15,
          }}>
          <FontAwesome name="paper-plane" size={25} color="black" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  btnSend: {
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    // backgroundColor: "7FBCD2",
    // ...getShadow(),
    borderRadius: 50,
  },
});
