import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Alert, Image, Linking, Text, TouchableOpacity} from 'react-native';

export default function RenderHotCollege(props) {
  //   console.log(props.data);
  const navigation = useNavigation();
  const xemThemText = text => {
    // const xemThem = '... Xem thêm';
    // let temp = '';
    // if (text.length >= 30) {
    //   temp = text.slice(0, 30);
    // }
    return (
      <Text style={{fontSize: 18, fontWeight: '600'}}>
        {text}
        {/* <Text style={{color: 'blue'}}>{xemThem}</Text> */}
      </Text>
    );
  };
  const handlePress = id => {
    navigation.push('CollegesDetail', {collegeId: id});
  };
  if (props.data)
    return props.data.map((item, index) => {
   
      return (
        <TouchableOpacity
          onPress={() => handlePress(item.collegesId)}
          key={index}
          style={{
            backgroundColor: 'white',
            height: 150,
            width: 200,
            borderRadius: 20,
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            margin: 5,
            justifyContent: 'flex-start',
            alignItems: 'center',
            overflow: 'hidden',
          }}>
          <Image
            style={{width: '100%', height: '50%'}}
            source={{
              uri: item.image
                ? item.image
                : 'https://www.caryinstitute.org/sites/default/files/public/styles/card_thumb/public/2019-09/icon_lesson_plan.png   ',
            }}
          />
          {xemThemText(item.name)}
        </TouchableOpacity>
      );
    });
  else return <></>;
}
