import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  Dimensions,
  ScrollView,
  BackHandler,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Octicons from 'react-native-vector-icons/Octicons';
import API from '../API/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken} from '../Constant-Storage';

export default function BlogDetail({navigation, route}) {
  const {blogID} = route.params;
  const [dataBlog, setDataBlog] = useState(null);
  const [loading, isLoading] = useState(false);

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.goBack();
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  useEffect(() => {
    const detailBlog = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `blogs/${blogID}/detail`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            console.log(res.data);
            setDataBlog(res.data);
          }
        })
        .catch(err => {
          alert(err);
        });
    };

    detailBlog();
  }, []);

  const likePress = async isReacted => {
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    await api
      .onCallAPI(
        'put',
        `blogs/${blogID}/react`,
        {},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(res => {
        if (res.data) {
          const temp = {...dataBlog};
          temp.isReacted = true;
          temp.numOfReact++;
          setDataBlog(temp);
        }
      })
      .catch(err => {
        alert(err);
      });
  };
  if (dataBlog)
    return (
      <View style={{flex: 1, position: 'relative'}}>
        <ImageBackground
          source={require('../../assets/blogBackGr.jpg')}
          resizeMode="stretch"
          style={{
            overflow: 'hidden',
            height: 70,
            justifyContent: 'center',
            marginBottom: 20,
          }}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
            }}>
            <View style={{flex: 1, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{flex: 1, justifyContent: 'center'}}>
                <Ionicons name="arrow-back-outline" size={35} color="white" />
              </TouchableOpacity>
            </View>
            <View style={{flex: 3, alignItems: 'center'}}>
              <Text
                style={{
                  fontSize: 32,
                  color: 'white',
                  textAlign: 'center',
                  alignSelf: 'center',
                }}>
                Blog Box
              </Text>
            </View>
            <View style={{flex: 0.5, alignItems: 'flex-end'}}></View>
          </View>
        </ImageBackground>
        <View
          style={{
            marginHorizontal: 5,
            marginBottom: 5,
            flexDirection: 'row',
            marginLeft: 10,
          }}>
          <Image
            source={
              dataBlog.ownerAvatar
                ? {uri: dataBlog.ownerAvatar}
                : {
                    uri: 'https://i.chungta.vn/2020/02/13/fpt2020-1581568294.jpg',
                  }
            }
            style={{
              width: 50,
              height: 50,
              borderRadius: 25,
            }}
          />
          <View>
            <Text
              style={{
                color: 'black',
                fontSize: 20,
                textAlign: 'left',
                paddingLeft: 20,
              }}>
              Đăng bởi:
            </Text>
            <Text
              style={{
                color: 'black',
                fontSize: 20,
                textAlign: 'left',
                paddingLeft: 20,
              }}>
              {dataBlog.ownerName}
            </Text>
          </View>
        </View>
        <View
          style={{
            marginHorizontal: 5,
            marginBottom: 10,
          }}>
          <Text style={{textAlign: 'left', fontSize: 22, color: 'black'}}>
            {dataBlog.title}
          </Text>
        </View>
        <View
          style={{
            marginHorizontal: 10,
            backgroundColor: 'white',
            height: 220,
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            borderRadius: 20,
            marginBottom: 20,
          }}>
          <Image
            source={
              dataBlog.blogImage
                ? {uri: dataBlog.blogImage}
                : {
                    uri: 'https://i.chungta.vn/2020/02/13/fpt2020-1581568294.jpg',
                  }
            }
            style={{
              width: '100%',
              height: '100%',
              borderRadius: 20,
            }}
          />
        </View>
        <ScrollView
          style={{
            marginHorizontal: 10,
            marginBottom: 10,
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
          }}>
          <View style={{flex: 1}}>
            <Text style={{textAlign: 'left', fontSize: 20, color: 'black'}}>
              {dataBlog.description}
            </Text>
          </View>
        </ScrollView>
        <View
          style={{
            height: 60,
            width: '100%',
            backgroundColor: 'white',
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            justifyContent: 'center',
            borderTopWidth: 1,
            borderColor: 'blue',
            flexDirection: 'row',
            position: 'absolute',
            bottom: 0,
          }}>
          <View
            style={{
              flex: 1,
              height: '100%',
              width: '100%',
              alignItems: 'flex-start',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              onPress={() => likePress(dataBlog.isReacted)}
              style={{alignSelf: 'center'}}>
              <AntDesign
                name={dataBlog.isReacted ? 'like1' : 'like2'}
                size={32}
                color="#7FBCD2"
              />
            </TouchableOpacity>
            <Text
              style={{
                color: '#7FBCD2',
                fontSize: 25,
                paddingLeft: 10,
                alignSelf: 'center',
              }}>
              Like
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              height: '100%',
              width: '100%',
              alignItems: 'flex-end',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              onPress={() => navigation.push('CommentBlog', {blogID})}
              style={{alignSelf: 'center'}}>
              <Octicons name="comment" size={32} color="#7FBCD2" />
            </TouchableOpacity>
            <Text
              style={{
                color: '#7FBCD2',
                fontSize: 25,
                paddingLeft: 10,
                alignSelf: 'center',
              }}>
              Comment
            </Text>
          </View>
        </View>
      </View>
    );
  else return <></>;
}
