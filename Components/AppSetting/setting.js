import {View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function Setting({navigation}) {
  const ScreenWidth = Dimensions.get('screen').width;
  const ScreenHeight = Dimensions.get('screen').height;

  return (
    <View style={{flex: 1, position: 'relative'}}>
      <View
        style={{
          height: ScreenHeight * 0.1,
          backgroundColor: '#F07122D4',
          justifyContent: 'center',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          style={{
            position: 'absolute',
            left: 0,
            paddingLeft: 10,
            alignSelf: 'center',
          }}
          onPress={() => navigation.goBack()}>
          <AntDesign name="leftcircle" size={40} color="white" />
        </TouchableOpacity>
        <Text
          style={{
            fontSize: 30,
            color: 'white',
            textAlign: 'center',
            alignSelf: 'center',
          }}>
          Cài đặt tùy chọn
        </Text>
      </View>
      {/*design body*/}
      <View style={{flex: 1, margin: 20, backgroundColor: 'transparent'}}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            width: '100%',
            height: ScreenHeight * 0.1,           
            borderRadius: 20,
            alignItems: 'center',
            marginVertical: 10,
            backgroundColor: 'white',
            elevation: 20,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
          }}>
            <TouchableOpacity style={{paddingLeft: 10}}>
              <Ionicons name='ios-notifications-circle-outline' size={40} color="#ff8c00"/>
            </TouchableOpacity>
            <View style={{paddingLeft: 20}}>
              <Text style={{fontSize: 25 ,color: 'black'}}>
                Mở thông báo
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
          style={{
            flexDirection: 'row',
            width: '100%',
            height: ScreenHeight * 0.1,
            borderRadius: 20,
            alignItems: 'center',
            marginVertical: 10,
            backgroundColor: 'white',
            elevation: 20,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
          }}>
            <TouchableOpacity style={{paddingLeft: 10}}>
              <Ionicons name='ios-notifications-off-circle-outline' size={40} color="#ff8c00"/>
            </TouchableOpacity>
            <View style={{paddingLeft: 20}}>
              <Text style={{fontSize: 25 ,color: 'black'}}>
                Đóng thông báo
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
          style={{
            flexDirection: 'row',
            width: '100%',
            height: ScreenHeight * 0.1,
            borderRadius: 20,
            alignItems: 'center',
            marginVertical: 10,
            backgroundColor: 'white',
            elevation: 20,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
          }}>
            <TouchableOpacity style={{paddingLeft: 10}}>
              <MaterialCommunityIcons name='theme-light-dark' size={40} color="#ff8c00"/>
            </TouchableOpacity>
            <View style={{paddingLeft: 20}}>
              <Text style={{fontSize: 25 ,color: 'black'}}>
                Xem ở màn hình tối
              </Text>
            </View>
          </TouchableOpacity>        
      </View>
    </View>
  );
}
