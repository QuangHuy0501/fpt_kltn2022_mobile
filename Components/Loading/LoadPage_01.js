import {
  View,
  Button,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default function LoadPage_01() {
  const navigation = useNavigation();

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  return (
    <ScrollView>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View style={{paddingVertical: 20}}>
          <Image
            source={require('../../assets/HollandImage.jpg')}
            resizeMode="contain"
            style={{width: 300, height: 350}}
          />
        </View>
        <Text
          style={{
            marginBottom: 5,
            padding: 10,
            textAlign: 'justify',
            fontSize: 18,
          }}>
          Trắc nghiệm tính cách nghề nghiệp Holland chỉ ra rằng con người với
          những kiểu tính cách khác nhau thích làm việc với những tác nhân kích
          thích khác nhau và khoảng cách giữa tính cách trong công việc cho thấy
          mức độ khác nhau trong sở thích của họ.
        </Text>

        <Text style={{padding: 10, textAlign: 'justify', fontSize: 18}}>
          <Text style={{fontWeight: 'bold'}}>John Holland</Text> đã sắp xếp sáu
          loại tính cách này vào một lục giác (nhìn biểu đồ) dựa trên sở thích
          làm việc với những tác nhân kích thích khác nhau gồm: con người, dữ
          liệu, đồ vật và ý tưởng.
        </Text>
        <View
          style={{
            flexDirection: 'row',
            paddingBottom: 20,
            paddingHorizontal: 20,
          }}>
          <View style={{flex: 1, alignItems: 'flex-start'}}>
            <TouchableOpacity
              style={{}}
              onPress={() =>
                navigation.push('mainBoard', {
                  avtURL: '',
                  userName: '',
                })
              }>
              <AntDesign name="leftcircle" size={40} color="#F07122" />
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity
              style={{}}
              onPress={() => navigation.navigate('Load02')}>
              <AntDesign name="rightcircle" size={40} color="#F07122" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}
