import {
  View,
  Text,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import React, {useEffect} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default function LoadPage_02({navigation}) {

  useEffect(() => {
    const backAction = async () => {
      try {
          navigation.goBack();
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);
  const ScreenWidth = Dimensions.get('screen').width;
  return (
    <ScrollView>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Image
          source={require('../../assets/GustavImage.jpg')}
          resizeMode="contain"
          style={{width: 300, height: 350, marginBottom: 5}}
        />
        <Text
          style={{
            marginBottom: 5,
            padding: 10,
            textAlign: 'justify',
            fontSize: 18,
            fontFamily: 'Arial',
          }}>
          Trắc nghiệm tính cách MBTI (Myers-Briggs Type Indicator) là một phương
          pháp sử dụng hàng loạt các câu hỏi trắc nghiệm để phân tích tính cách
          con người.
        </Text>
        <Text style={{fontSize: 18, padding: 10, textAlign: 'justify'}}>
          Trắc nghiệm tính cách MBTI đặc biệt nhấn mạnh vào sự khác biệt về mặt
          tự nhiên của từng người dựa trên tứng câu trả lời của họ cho các câu
          hỏi để suy ra những cá tính, tính cách riêng biệt của từng người. nhà
          tuyển dụng cũng có thể sử dụng MBTI để đánh giá mức độ phù hợp về tính
          cách của ứng viên với công việc cũng như môi trường làm việc của doanh
          nghiệp.
        </Text>
        <View
          style={{
            flexDirection: 'row',
            paddingBottom: 20,
            paddingHorizontal: 20,
          }}>
          <View style={{flex: 1, alignItems: 'flex-start'}}>
            <TouchableOpacity style={{}} onPress={() => navigation.goBack()}>
              <AntDesign name="leftcircle" size={40} color="#F07122" />
            </TouchableOpacity>
          </View>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity
              style={{}}
              onPress={() => navigation.navigate('ChooseTestPage')}>
              <AntDesign name="rightcircle" size={40} color="#F07122" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}
