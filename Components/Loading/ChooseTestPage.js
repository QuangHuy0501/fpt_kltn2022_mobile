import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import TestPage_01 from '../DoTest/TestPage_01';
import StartPage from '../StartPage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AntDesign from 'react-native-vector-icons/dist/AntDesign';
import API from '../API/api';
import {
  currentStack,
  Current_Screen,
  Current_Screen_Params,
  Grade_Arrays_Screen,
  Holland_Answers_Array,
  Holland_Questions_Array,
  Holland_Result_Array,
  indexQuestionMBTI,
  LoginAppToken,
  questionID_MBTI,
} from '../Constant-Storage';
const Stack = createNativeStackNavigator();

export default function ChooseTestPage() {
  const navigation = useNavigation();

  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const getHollandTest = async () => {
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    api
      .onCallAPI(
        'get',
        'questions/holland',
        {},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(async res => {
        if (res.data) {
          const test = {
            a: await AsyncStorage.getItem(Current_Screen),
            b: await AsyncStorage.getItem(Current_Screen_Params),
            c: await AsyncStorage.getItem(Holland_Answers_Array),
            d: await AsyncStorage.getItem(Holland_Questions_Array),
            e: await AsyncStorage.getItem(currentStack),
            f: await AsyncStorage.getItem(Holland_Result_Array),
          };
          console.log(test);
          // return;
          const curScreen = await AsyncStorage.getItem(currentStack);
          if (curScreen === 'DoTest01') {
            let page = await AsyncStorage.getItem(Current_Screen_Params);
            if (page === '') page = 0;
            console.log(page);

            page = parseInt(page);
            if (page === 1) {
              const data = await AsyncStorage.getItem(Holland_Answers_Array);
              console.log(JSON.parse(data));
              // return;

              navigation.navigate('DoTest01', {
                dataHollandTest: JSON.parse(data),
                page: page,
              });
              return;
            }
            let questionData = await AsyncStorage.getItem(
              Holland_Questions_Array,
            );
            // let answersData = await AsyncStorage.getItem(Holland_Answers_Array);
            ///neu answer đang ở trang mss chưa trả lời thì data cần gửi là question, còn chọn câu trl r thì answer
            // const pbiet = {
            //   a: JSON.parse(questionData),
            //   b: JSON.parse(answersData),
            // };
            // console.log(pbiet);
            // // return;
            // if (pbiet.a[0].id === pbiet.b[0]?.id) questionData = pbiet.b;
            // else questionData = pbiet.a;
            // const temp = questionData.slice((page - 1) * 10 + 1, page * 10);
            navigation.navigate('DoTest01', {
              dataHollandTest: JSON.parse(questionData),
              page: page + 1,
            });
            return;
          }
          if (curScreen === 'personalityGrResult') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = parseInt(screenParam);
            navigation.navigate('personalityGrResult', {
              testid: screenParam,
            });
            return;
          }
          if (curScreen === 'MajorResult') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = parseInt(screenParam);
            navigation.navigate('MajorResult', {
              group_id: screenParam,
            });
            return;
          }
          if (curScreen === 'InputGrade') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = parseInt(screenParam);
            navigation.navigate('InputGrade', {
              majorID: screenParam,
            });
            return;
          }
          if (curScreen === 'UniversityScreen') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = parseInt(screenParam);
            navigation.navigate('UniversityScreen');
            return;
          }
          if (curScreen === 'InputGPA') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = JSON.parse(screenParam);
            // screenParam = Object.(screenParam);
            navigation.navigate('InputGPA', screenParam);
            return;
          }
          const dataQuestion = res.data.slice(0, 10);
          console.log('h2');
          navigation.navigate('DoTest01', {
            dataHollandTest: dataQuestion,
            page: 1,
          });
        }
      })
      .catch(err => alert(err));
  };
  const getMBTITest = async () => {
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    api
      .onCallAPI(
        'get',
        'questions/all_mbti_id',
        {},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(async res => {
        if (res.data) {
          const curScreen = await AsyncStorage.getItem(currentStack);
          if (curScreen === 'personalityGrResult') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = parseInt(screenParam);
            navigation.navigate('personalityGrResult', {
              testid: screenParam,
            });
            return;
          }
          if (curScreen === 'MajorResult') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = parseInt(screenParam);
            navigation.navigate('MajorResult', {
              group_id: screenParam,
            });
            return;
          }
          if (curScreen === 'InputGrade') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = parseInt(screenParam);
            navigation.navigate('InputGrade', {
              majorID: screenParam,
            });
            return;
          }
          if (curScreen === 'UniversityScreen') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = parseInt(screenParam);
            navigation.navigate('UniversityScreen');
            return;
          }
          if (curScreen === 'InputGPA') {
            let screenParam = await AsyncStorage.getItem(Current_Screen_Params);
            screenParam = JSON.parse(screenParam);
            // screenParam = Object.(screenParam);
            navigation.navigate('InputGPA', screenParam);
            return;
          }
          const mbtiQuestion = await AsyncStorage.getItem(questionID_MBTI);
          let questionID = parseInt(mbtiQuestion);
          const isLargeNumber = element => element === questionID;
          let indexQuestion = res.data.findIndex(isLargeNumber);

          if (indexQuestion < 0 || !indexQuestion) indexQuestion = 0;
          await AsyncStorage.setItem(
            indexQuestionMBTI,
            indexQuestion.toString(),
          );
          // let questionID = parseInt(mbtiQuestion);
          if (!questionID) questionID = 1;
          navigation.push('DoTest02', {
            IDsMBTI: res.data,
            questionID: res.data[indexQuestion],
          });
        }
      })
      .catch(err => alert(err));
  };

  return (
    <ScrollView>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Image
          source={require('../../assets/HollandTestWelcome.jpg')}
          resizeMode="contain"
          style={{width: 400, height: 250, marginTop: 20}}
        />
        <View>
          <TouchableOpacity
            style={{alignItems: 'center'}}
            onPress={() => getHollandTest()}>
            <View
              style={{
                width: 250,
                height: 50,
                backgroundColor: '#F07122',
                borderRadius: 30,
                justifyContent: 'center',
              }}>
              <Text style={{color: 'white', fontSize: 25, textAlign: 'center'}}>
                Holland Test
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <Image
          source={require('../../assets/MBTITestWelcome.jpg')}
          resizeMode="contain"
          style={{width: 400, height: 250, marginTop: 30, marginBottom: 10}}
        />
        <View>
          <TouchableOpacity
            style={{alignItems: 'center'}}
            onPress={getMBTITest}>
            <View
              style={{
                width: 250,
                height: 50,
                backgroundColor: '#F07122',
                marginBottom: 30,
                borderRadius: 30,
                justifyContent: 'center',
              }}>
              <Text style={{color: 'white', fontSize: 25, textAlign: 'center'}}>
                MBTI Test
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{flexDirection: 'row', paddingBottom: 20, paddingLeft: 20}}>
        <TouchableOpacity
          style={{flex: 1, alignItems: 'flex-start'}}
          onPress={async () => {
            navigation.goBack();
            // await AsyncStorage.setItem(Current_Screen, '');
            // await AsyncStorage.setItem(Current_Screen_Params, '');
            // await AsyncStorage.setItem(Holland_Answers_Array, '');
            // await AsyncStorage.setItem(Holland_Questions_Array, '');
            // await AsyncStorage.setItem(Current_Screen_Params, '');
            // await AsyncStorage.setItem(Holland_Result_Array, '');
            // await AsyncStorage.setItem(currentStack, '');
          }}>
          <AntDesign name="leftcircle" size={40} color="#F07122" />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}
