import {View, Text, ActivityIndicator} from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

export default function Spiner() {
  return (
    //màu nên của spiner (nền dưới)
    <View style={{flex: 1, backgroundColor: '#BBDEFB'}}>
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          justifyContent: 'center',
          alignContent: 'center',
          width: 160,
          height: 160,
          zIndex: 999,
          position: 'absolute',
          top: '30%',
          alignSelf: 'center',
          backgroundColor: '#BBDEFB', //màu nên chữ của spiner
          borderColor: 'white',
          borderWidth: 1,
        }}>
        <View style={{}}>
          <ActivityIndicator size="large" color="black" />
          <Text
            style={{
              textAlign: 'center',
              fontSize: 20,
              fontWeight: 'bold',
              color: 'black',
            }}>
            Vui lòng đợi
          </Text>
        </View>
      </LinearGradient>
    </View>
  );
}

