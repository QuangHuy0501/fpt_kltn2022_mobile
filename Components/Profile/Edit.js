import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Keyboard,
  Alert,
  BackHandler,
} from 'react-native';
import React, {useState, useEffect, useRef} from 'react';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {SafeAreaView} from 'react-native-safe-area-context';
import ImagePicker from 'react-native-image-crop-picker';
import {TextInput} from 'react-native-paper';
import Modal from 'react-native-modal';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LoginAppToken,
  avtURL,
  userFullName,
  userEmail,
  UserIDApp,
  firestoreUID,
} from '../Constant-Storage';
import Spiner from '../Loading/Spiner';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import API from '../API/api';
import storage from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';
import DatePicker from 'react-native-datepicker';

export default function EditProfile({navigation, route}) {
  const [userName, setUserName] = useState('');
  const [disPlayBirthday, setDisPlayBirthday] = useState('');
  const [isSelected, setIsSelected] = useState(-1); //gender
  const [imagePath, setimagePath] = useState('');
  const [displayPhone, setDisplayPhone] = useState('');
  const [imageObject, setimageObject] = useState(undefined);
  const previousBirthday = useRef('');

  const [fullName, setFullName] = useState('');
  const [gender, setGender] = useState('');
  const [phone, setPhone] = useState('');
  const [birthday, setBirthday] = useState('');
  const [keyboardStatus, setKeyboardStatus] = useState(false);
  const [visible, setVisible] = useState(false);
  const radio_props = [
    {label: 'Nữ', value: 0},
    {label: 'Nam', value: 1},
  ];

  const [date, setDate] = useState('09-29-2022');
  // const [image, setImage] = useState(null);
  useEffect(() => {
    const backAction = async () => {
      try {
        navigation.push('mainBoard', {
          avtURL: '',
          userName: '',
        });
      } catch (err) {
        console.log(err);
      }
    };
    BackHandler.removeEventListener('hardwareBackPress');
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const [post, setPost] = useState(null);
  const phoneRef = useRef(null);
  const checkPhoneNum = phoneNum => {
    if (phoneNum < 0) {
      Alert.alert('Thông báo', 'Số điện thoại không được âm', [
        {
          text: 'OK',
          onPress: () => {
            setPhone('');
          },
        },
      ]);
      return;
    }
    setPhone(phoneNum);
    if (phoneNum.length > 10) {
      Alert.alert('Thông báo', 'Số điện thoại phải là 10 số', [{text: 'OK'}]);
    }
  };
  const birthDayPress = key => {
    const temp = previousBirthday.current;
    if (key === 'Backspace') {
      let test = previousBirthday.current.substring(0, temp.length - 1);
    }
  };
  const birthDayChange = text => {
    // let birthDay = [...text];
    if (text.length === 2) {
      text = `${text}/`;
    }
    if (text.length === 5) {
      text = `${text}/`;
    }
    if (text.length > 10) return;
    setBirthday(text);
    previousBirthday.current = text.replace('/', '');
  };

  const submitPost = async () => {
    if (!imageObject) return;

    const uploadImage = imagePath;
    const filename = uploadImage.substring(uploadImage.lastIndexOf('/') + 1);
    const userName = await AsyncStorage.getItem(userFullName);
    try {
      const storageRef = await storage().ref(`${userName}/${filename}`);
      await storageRef.putFile(uploadImage);
      const imageURL = await storageRef.getDownloadURL();
      //link hình trên file base
      setimagePath(imageURL);
      await AsyncStorage.setItem(avtURL, imageURL);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardStatus(true);
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardStatus(false);
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  useEffect(() => {
    async function userInfo() {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      console.log(tokenDnhap);
      const api = new API();
      api
        .onCallAPI(
          'get',
          `sys_users/profile`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(async res => {
          if (res.data) {
            console.log('data nguoi dung', res.data);
            if (res.data.birthDate) {
              const dateShow = res.data.birthDate.slice(0, 10);
              const [year, month, day] = dateShow.split('-');
              setDisPlayBirthday(day + '/' + month + '/' + year);
            }

            setimagePath(res.data.imagePath);
            setDisplayPhone(res.data.phoneNumber);
            setIsSelected(res.data.gender ? 1 : 0);
            setUserName(res.data.fullName);
            // setDisPlayBirthday(res.data.birthDate);
          }
        })
        .catch(err => alert(err));
      // const userAvatar = await AsyncStorage.getItem(avtURL); //res.data.imagePath;
      // const userDisplayName = await AsyncStorage.getItem(userFullName); //res.data.fullName;
      // const userDisplayEmail = await AsyncStorage.getItem(userEmail); //res.data.email;

      // }
      // })
      // .catch(err => console.log(err));
    }
    userInfo();
  }, []);

  const SaveInfoUpdate = async () => {
    if (phone.length < 10 && phone.length > 0) {
      Alert.alert('Thông báo', 'Số điện thoại phải là 10 số', [{text: 'OK'}]);
      phoneRef.current.focus();
      return;
    }
    setVisible(true);
    await submitPost();
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    const imageUrl = await AsyncStorage.getItem(avtURL);

    const logval = {
      fullName: fullName.length === 0 ? userName : fullName,
      gender: isSelected == 1 ? true : isSelected === 0 ? false : true,
      phoneNumber: phone.length === 0 ? displayPhone : phone,
      birthDay: birthday.length === 0 ? disPlayBirthday : birthday,
      imagePath: imageUrl,
    };
    const [day, month, year] = logval.birthDay.split('/');
    const birthDateParam = year + '-' + month + '-' + day + 'T00:00:00';
    logval.birthDay = birthDateParam;
    console.log(JSON.stringify(logval));
    // return;
    api
      .onCallAPI(
        'put',
        `sys_users/profile`,
        logval,
        {},
        {
          Authorization: 'bearer ' + tokenDnhap,
        },
      )
      .then(async res => {
        if (res.data) {
          Alert.alert('Thông tin cá nhân', 'Đã cập nhật thành công', [
            {text: 'OK', onPress: () => navigation.push('HomeScreen')},
          ]);
          //console.log(res.data);
          setimagePath(userAvatar);
          setUserName(userDisplayName);
          setDisPlayBirthday(disPlayBirthday);
        }
      })
      .catch(err => alert(err));
    const userAvatar = await AsyncStorage.getItem(avtURL); //res.data.imagePath;
    const userDisplayName = await AsyncStorage.getItem(userFullName); //res.data.fullName;
    const userDisplayEmail = await AsyncStorage.getItem(userEmail); //res.data.email;
    setimagePath(userAvatar);
    setUserName(userDisplayName);
    setDisPlayBirthday(disPlayBirthday);
    setVisible(false);
    // }
    // })
    // .catch(err => console.log(err));
  };

  const pickSingle = (cropit, circular = false, mediaType) => {
    // funtion mở internal storage để người dùng chọn hình ảnh
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      elevation: 50,
      shadowOffset: {width: 20, height: 20},
      shadowColor: 'black',
    })
      .then(image => {
        setimagePath(image.path);
        setimageObject(image);
      })
      .catch(err => console.log(err));
  };
  if (imagePath.length === 0 || visible) return <Spiner />;
  return (
    <View style={{flex: 1, position: 'relative'}}>
      {/*design header trang*/}
      <View style={{height: 150, justifyContent: 'center'}}>
        <SafeAreaView style={{}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              //justifyContent: 'space-between',
            }}>
            <View style={{flex: 1, alignItems: 'flex-start'}}>
              <TouchableOpacity
                onPress={() =>
                  navigation.push('mainBoard', {
                    avtURL: '',
                    userName: '',
                  })
                }
                style={{flex: 1}}>
                <Ionicons name="arrow-back-outline" size={32} color="#F07122" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 2,
                fontSize: 34,
                color: '#F07122',
                textAlign: 'center',
                paddingTop: 30,
              }}>
              THÔNG TIN CÁ NHÂN
            </Text>
            <View style={{flex: 1, alignItems: 'flex-end'}}></View>
          </View>
        </SafeAreaView>
      </View>
      {/*design phần thân giữa trang*/}
      <View style={{flex: 4}}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <View style={{flex: 1, marginHorizontal: 20}}>
            {/*design ô image picker*/}
            {imagePath ? (
              <TouchableOpacity
                onPress={() => pickSingle(true)}
                style={{
                  width: 120,
                  height: 120,
                  borderRadius: 60,
                  elevation: 30,
                  shadowOffset: {width: 100, height: 100},
                  shadowColor: 'black',
                  alignSelf: 'center',
                  marginBottom: 20,
                  overflow: 'hidden',
                }}>
                <Image
                  source={{uri: imagePath}}
                  resizeMode="cover"
                  style={{
                    flex: 1,
                  }}
                />
              </TouchableOpacity>
            ) : (
              <Spiner />
            )}

            {/*design phần textinput*/}
            <View style={{marginBottom: 20}}>
              <Text style={{fontSize: 22, color: '#F07122D4', marginBottom: 5}}>
                Họ và tên
              </Text>
              <TextInput
                // label={userName}
                style={{fontSize: 22}}
                value={fullName}
                placeholder={userName}
                onSubmitEditing={Keyboard.dismiss}
                onChangeText={fullName => setFullName(fullName)}
              />
            </View>
            <View style={{marginBottom: 20}}>
              <Text style={{fontSize: 22, color: '#F07122D4', marginBottom: 5}}>
                Giới tính
              </Text>
              <RadioForm
                initial={isSelected}
                formHorizontal={false}
                animation={true}
                style={{width: '90%', alignSelf: 'center'}}>
                {radio_props.map((arr, i) => {
                  return (
                    <RadioButton
                      labelHorizontal={true}
                      key={i}
                      style={{
                        width: '100%',
                        position: 'relative',
                        borderBottomColor: 'rgba(0,0,0,0.5)',
                        borderBottomWidth: 1,
                        paddingVertical: 20,
                      }}>
                      <RadioButtonLabel
                        obj={arr}
                        index={i}
                        labelHorizontal={true}
                        onPress={() => {
                          setIsSelected(i);
                        }}
                        labelStyle={{
                          fontSize: 20,
                          color: '#000',
                          paddingTop: 5,
                        }}
                        labelWrapStyle={{}}
                      />
                      <RadioButtonInput
                        obj={arr}
                        index={i}
                        isSelected={isSelected === i}
                        onPress={() => {
                          setIsSelected(i);
                        }}
                        borderWidth={1}
                        buttonInnerColor={
                          isSelected === i ? '#1877F2' : 'rgba(0,0,0,0.8)'
                        } //chấm tròn trong
                        buttonOuterColor={
                          isSelected === i ? '#1877F2' : 'rgba(0,0,0,0.5)'
                        } // màu viền
                        buttonSize={10}
                        buttonOuterSize={20}
                        buttonStyle={{}}
                        buttonWrapStyle={{
                          position: 'absolute',
                          right: 0,
                          bottom: 20,
                        }}
                      />
                    </RadioButton>
                  );
                })}
              </RadioForm>
            </View>
            <View style={{marginBottom: 20}}>
              <Text style={{fontSize: 22, color: '#F07122D4', marginBottom: 5}}>
                Điện thoại
              </Text>
              <TextInput
                //label="Phone"
                placeholder={displayPhone}
                style={{}}
                value={phone}
                onSubmitEditing={Keyboard.dismiss}
                onChangeText={phone => checkPhoneNum(phone)}
                keyboardType="numeric"
                ref={phoneRef}
              />
            </View>
            <View style={{marginBottom: 20}}>
              <Text style={{fontSize: 22, color: '#F07122D4', marginBottom: 5}}>
                Ngày sinh
              </Text>
              <TextInput
                //label="BirthDay"
                placeholder={disPlayBirthday}
                keyboardType="numeric"
                style={{}}
                value={birthday}
                onSubmitEditing={Keyboard.dismiss}
                onKeyPress={e => birthDayPress(e.nativeEvent.key)}
                onChangeText={birthday => birthDayChange(birthday)}
              />
              {/* <DatePicker
                style={styles.datePickerStyle}
                date={date} // Initial date from state
                mode="date" // The enum of date, datetime and time
                placeholder="select date"
                format="DD-MM-YYYY"
                minDate="01-01-2016"
                maxDate="01-01-2019"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    //display: 'none',
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 36,
                  },
                }}
                onDateChange={date => {
                  setDate(date);
                }}
              /> */}
            </View>
          </View>
        </ScrollView>
        {/*design bottom bar*/}
        <View
          style={{
            justifyContent: 'center',
            alignSelf: 'center',
            height: 50,
            width: 200,
            marginVertical: 10,
            marginLeft: 15,
            borderRadius: 20,
            elevation: 50,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            backgroundColor: '#ABD9FF',
            display: keyboardStatus ? 'none' : 'flex',
          }}>
          <TouchableOpacity
            onPress={SaveInfoUpdate}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: 'black', fontSize: 22}}>Lưu thay đổi</Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <TouchableOpacity
          onPress={SaveInfoUpdate}
          style={{
            width: 80,
            height: 80,
            borderRadius: 40,
            display: keyboardStatus ? 'none' : 'flex',
            backgroundColor: '#F07122',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <FontAwesome name="save" size={40} color="#ffffff" />
        </TouchableOpacity>
      </View> */}
    </View>
  );
}
