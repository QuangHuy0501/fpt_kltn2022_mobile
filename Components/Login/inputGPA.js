import {
  View,
  Text,
  Dimensions,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {TextInput} from 'react-native-paper';
import AntDesign from 'react-native-vector-icons/AntDesign';
import API from '../API/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  currentStack,
  Current_Screen_Params,
  Grade_Arrays_Screen,
  LoginAppToken,
} from '../Constant-Storage';
import Spiner from '../Loading/Spiner';
import LinearGradient from 'react-native-linear-gradient';

export default function InputGPA({navigation, route}) {
  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardStatus(true);
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardStatus(false);
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);
  //nhận params grade từ InputGrade screen
  // const group_id = 1;
  // const majorID = 2;
  const {group_id, majorID} = route.params;
  const ScreenWidth = Dimensions.get('screen').width;
  const ScreenHeight = Dimensions.get('screen').height;
  const [gpa, setGPA] = useState([]); //dạng array
  const [keyboardStatus, setKeyboardStatus] = useState(false);
  const [subjects, setSubjects] = useState();
  // {
  //   "id": 9,
  //   "name": "Toán"
  // }
  useEffect(() => {
    const getSubject = async () => {
      await AsyncStorage.setItem(currentStack, 'InputGPA');
      const grade = await AsyncStorage.getItem(Grade_Arrays_Screen);
      const gpaArr = JSON.parse(grade);
      if (gpaArr) setGPA(gpaArr);
      await AsyncStorage.setItem(
        Current_Screen_Params,
        JSON.stringify(route.params),
      );
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      api
        .onCallAPI(
          'get',
          `subject_groups/${group_id}/subject`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            setSubjects(res.data);
            console.log(res.data);
          }
        })
        .catch(err => alert(err));
    };
    getSubject();
  }, []);

  const inputGPAView = grade => {
    return grade.map((item, i) => {
      return (
        <View
          key={i}
          style={{
            marginTop: 20,
            paddingHorizontal: 20,
          }}>
          <View style={{paddingBottom: 10}}>
            <Text style={{fontSize: 20, color: 'black', fontWeight: '400'}}>
              Điểm {item.name}:
            </Text>
            <TextInput
              //onFocus={() => this.onFocus()}
              style={{backgroundColor: 'white', borderRadius: 20}}
              keyboardType="numeric"
              label="Nhập vào đây: (Vd: 7.6)"
              value={gpa[i]?.point.toString()}
              onSubmitEditing={Keyboard.dismiss}
              onChangeText={text => inputGPAText(i, text, item.id)}
            />
          </View>
        </View>
      );
    });
  };

  const inputGPAText = async (i, text, id) => {
    if (text < 0) {
      Alert.alert('Thông báo', 'Điểm số không được âm', [
        {
          text: 'OK',
          onPress: async () => {
            const temp = [...gpa];
            const value = {
              subjectId: id,
              point: '',
            };
            temp[i] = value;
            await AsyncStorage.setItem(
              Grade_Arrays_Screen,
              JSON.stringify(temp),
            );
            setGPA(temp);
          },
        },
      ]);
      return;
    }
    if (text > 10) {
      Alert.alert('Thông báo', 'Điểm số không được lớn hơn 10', [
        {
          text: 'OK',
          onPress: async () => {
            const temp = [...gpa];
            const value = {
              subjectId: id,
              point: '',
            };
            temp[i] = value;
            await AsyncStorage.setItem(
              Grade_Arrays_Screen,
              JSON.stringify(temp),
            );
            setGPA(temp);
          },
        },
      ]);
      return;
    }
    const temp = [...gpa];
    const value = {
      subjectId: id,
      point: text,
    };
    temp[i] = value;
    await AsyncStorage.setItem(Grade_Arrays_Screen, JSON.stringify(temp));
    setGPA(temp);
  };

  const sendGpa = async () => {
    //if()
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    const param = [];
    gpa.map(item => {
      item.point = parseFloat(item.point);
      param.push(item);
    });
    console.log(param);
    if (param.length < 3) {
      Alert.alert('Thông báo', 'Bạn phải nhập đầy đủ điểm số cả 3 khối', [
        {
          text: 'OK',
          onPress: async () => {
            const temp = [...gpa];
            const value = {
              subjectId: id,
              point: '',
            };
            temp[i] = value;
            await AsyncStorage.setItem(
              Grade_Arrays_Screen,
              JSON.stringify(temp),
            );
            setGPA(temp);
          },
        },
      ]);
      return;
    }
    //onst param ={listSubjrct:gpa}
    api
      .onCallAPI(
        'post',
        'sys_users/subject',
        {listSubject: param},
        {},

        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(res => {
        if (res.data) {
          navigation.navigate('UniversityScreen', {majorID: majorID});
        }
      })
      .catch(err => alert('lõi roi', err));
  };
  if (!subjects && gpa.length <= 0) return <Spiner />;
  if (subjects === undefined) return <Spiner />;
  console.log(subjects, gpa);
  return (
    <View style={{flex: 1, position: 'relative', backgroundColor: '#BBDEFB'}}>
      <View
        style={{
          marginTop: ScreenHeight * 0.1,
          marginLeft: 20,
          marginRight: 10,
        }}>
        <Text style={{fontSize: 30, color: 'black', fontWeight: 'bold'}}>
          Nhập vào điểm trung bình bài kiểm tra học kì gần nhất của bạn:
        </Text>
        <Text style={{fontSize: 20, fontWeight: '400'}}>
          Hãy nhập điểm của bạn 1 cách chính xác nhé, dữ liệu của bạn cần chính
          xác đấy!
        </Text>
      </View>
      <View style={{flex: 8}}>
        <ScrollView>{inputGPAView(subjects)}</ScrollView>
      </View>
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 20,
          bottom: 20,
          flex: 1,
          justifyContent: 'space-around',
          display: keyboardStatus ? 'none' : 'flex',
        }}>
        <LinearGradient
          start={{x: 0.25, y: 0.5}}
          end={{x: 1.0, y: 1.0}}
          locations={[0, 0.5, 0.6]}
          colors={['#ECA376', '#F07122', '#EE8543']}
          style={{
            height: 50,
            width: 300,
            backgroundColor: '#00ffff',
            // position: 'absolute',
            // bottom: 10,
            // right: 50,
            marginTop: 20,
            borderRadius: 20,
            elevation: 10,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            justifyContent: 'center',
            alignSelf: 'center',
          }}>
          <TouchableOpacity onPress={sendGpa} style={{alignSelf: 'center'}}>
            <Text
              style={{
                fontSize: 22,
                color: 'white',
                textAlign: 'center',
              }}>
              Xem trường phù hợp với bạn
            </Text>
          </TouchableOpacity>
        </LinearGradient>
      </View>
    </View>
  );
}
