import {View, Text, Dimensions, ImageBackground} from 'react-native';
import React from 'react';

export default function Wait() {
  const ScreenWidth = Dimensions.get('screen').width;
  const ScreenHeight = Dimensions.get('screen').height;

  return (
    <View style={{flex: 1, position: 'relative'}}>
      <ImageBackground
        style={{height: ScreenHeight, width: ScreenWidth, padding: 20}}
        resizeMode="contain"
        source={require('../../assets/apologyLogin.jpg')}>
        <View
          style={{
            marginTop: ScreenHeight * 0.05,
            marginHorizontal: 20,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 30,
              color: 'black',
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            Bạn vui lòng chờ giây lát trong thời gian xác thực. Chúng tôi xin
            lỗi vì sự bất tiện này
          </Text>
        </View>
      </ImageBackground>
    </View>
  );
}
