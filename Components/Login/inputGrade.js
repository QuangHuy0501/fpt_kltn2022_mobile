import {
  View,
  Text,
  Dimensions,
  Keyboard,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {TextInput} from 'react-native-paper';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  currentStack,
  Current_Screen_Params,
  LoginAppToken,
} from '../Constant-Storage';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import API from '../API/api';
import Spiner from '../Loading/Spiner';

export default function InputGrade({navigation, route}) {
  const {majorID} = route.params;
  //const majorID = 9;
  console.log(majorID);
  const ScreenWidth = Dimensions.get('screen').width;
  const ScreenHeight = Dimensions.get('screen').height;
  const [grade, setGrade] = useState();
  // const [keyboardStatus, setKeyboardStatus] = useState(false);

  useEffect(() => {
    const getSubjectGroup = async () => {
      await AsyncStorage.setItem(currentStack, 'InputGrade');
      await AsyncStorage.setItem(Current_Screen_Params, majorID.toString());
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `majors/${majorID}/subject_group`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            /// [{
            //  SubGrId: 1, groupName: "A1", Subject: [{id: 1, name Toán},...]
            // }]
            setGrade(res.data);
          }
        })
        .catch(err => alert(err));
    };
    getSubjectGroup();
    // const backAction = async () => {
    //   try {
    //     const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    //     await GoogleSignin.revokeAccess();
    //     await GoogleSignin.signOut();
    //     // setloggedIn(false);
    //     // setuserInfo([]);
    //     await AsyncStorage.clear();
    //     navigation.navigate('LoginPage');
    //   } catch (err) {
    //     console.log('loi log out' + err);
    //   }
    // };

    // const backHandler = BackHandler.addEventListener(
    //   'hardwareBackPress',
    //   backAction,
    // );

    // return () => backHandler.remove();
  }, []);
  const subGrPress = async group_id => {
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const api = new API();
    api
      .onCallAPI(
        'post',
        `sys_users/select_subject_group/${group_id}`,
        {},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(res => {
        if (res.data) {
          navigation.push('InputGPA', {group_id: group_id, majorID: majorID});
        }
      })
      .catch(
        err => alert(err),
        //navigation.push('InputGPA', {group_id: group_id, majorID: majorID}),
      );
    // console.log('xin chào');
  };
  if (!grade) return <Spiner />;
  return (
    <View style={{flex: 1, position: 'relative', backgroundColor: '#BBDEFB'}}>
      <View
        style={{
          marginTop: ScreenHeight * 0.1,
          marginLeft: 20,
          marginRight: 10,
        }}>
        <Text style={{fontSize: 30, color: 'black', fontWeight: 'bold'}}>
          Vui lòng chọn khối mà bạn muốn theo học :
        </Text>
        <Text style={{fontSize: 23, fontWeight: '600'}}>
          (Vui lòng nhấn vào khối để chọn)
        </Text>
      </View>
      <View style={{marginTop: 40, paddingHorizontal: 20}}>
        {grade.map((item, index) => {
          /// [{
          //  SubGrId: 1, groupName: "A1", Subject: [{id: 1, name Toán},...]
          // }]
          // day la dang gia lap, nhung bien nay se thay doi theo swagger, khi thay doi thi lum xuong
          return (
            <TouchableOpacity
              onPress={() => subGrPress(item.id)}
              key={index}
              style={{borderRadius: 40, padding: 20}}>
              <Text
                style={{
                  fontSize: 25,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  color: 'black',
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
      {/* <View style={{position: 'absolute', bottom: 20, right: 20}}>
        <TouchableOpacity
          style={{flex: 1, alignItems: 'flex-end'}}
          onPress={() => navigation.push('InputGPA', {grade: grade})}>
          <AntDesign name="rightcircle" size={50} color="#F07122" />
        </TouchableOpacity>
      </View> */}
    </View>
  );
}
