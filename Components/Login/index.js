import React, {useState, useEffect, useLayoutEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  Image,
  TouchableOpacity,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import LinearGradient from 'react-native-linear-gradient';
import API from '../API/api';
import {
  LoginAppToken,
  avtURL,
  userFullName,
  userEmail,
  majorUserChoose,
  IntroShow,
} from '../Constant-Storage';

export default function LoginPage({navigation}) {
  const signInWithGoogleAsync = async () => {
    // Get the users ID token
    // console.log('concac');
    GoogleSignin.configure({
      webClientId:
        'firebase-adminsdk-iyo41@personalstrength-83af0.iam.gserviceaccount.com',
    });
    const isIntro = await AsyncStorage.getItem(IntroShow);
    const majorID = await AsyncStorage.getItem(majorUserChoose);
    AsyncStorage.clear();
    const {idToken} = await GoogleSignin.signIn();
    console.log(idToken);
    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    const user_sign_in = await auth().signInWithCredential(googleCredential);
    const user = user_sign_in.user;
    const idTokenResult = user.getIdTokenResult();
    const loginEmail = user.email;
    //console.log(idToken);
    const tokenNe = (await idTokenResult).token;
    console.log(user_sign_in, tokenNe);

    //console.log(loginEmail);
    await AsyncStorage.setItem(userEmail, loginEmail);
    const api = new API();
    api
      .onCallAPI(
        'post',
        'sys_users/login_by_email',
        {},
        {firebaseToken: tokenNe},
        {},
      )
      .then(async res => {
        //lưu data res vào storage để giữ đăng nhập home
        console.log(res.data);
        await AsyncStorage.setItem(LoginAppToken, res.data.token); // lưu token response trả về để authorize
        await AsyncStorage.setItem(avtURL, res.data.avatar);
        await AsyncStorage.setItem(userFullName, res.data.fullname);
        // await AsyncStorage.setItem(UserIDApp, res.data.id.toString());
        if (majorID) await AsyncStorage.setItem(majorUserChoose, majorID);
        if (isIntro) await AsyncStorage.setItem(IntroShow, isIntro);

        navigation.push('mainBoard', {
          avtURL: res.data.avatar,
          userName: res.data.fullname,
        });
      })
      .catch(err => alert(err));
  };

  return (
    <View style={{flex: 1, position: 'relative'}}>
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={styles.linearGradient}>
        <Text
          style={{
            fontSize: 34,
            color: '#fff9e6',
            textAlign: 'center',
            paddingTop: 30,
          }}>
          Chào mừng{' '}
          <Text
            style={{
              fontSize: 34,
              color: '#fff9e6',
              textAlign: 'center',
              paddingTop: 30,
            }}>
            các bạn
          </Text>
        </Text>
        <View>
          <Text
            style={{
              fontSize: 22,
              color: 'white',
              textAlign: 'center',
              paddingTop: 30,
            }}>
            Hãy đến đây cùng chúng tôi để tìm hiểu chính xác hơn về bản thân.
          </Text>
        </View>
      </LinearGradient>
      <View style={{flex: 1, marginTop: 20, paddingBottom: 70}}>
        <Image
          source={require('../../assets/LoginBanner.jpg')}
          resizeMode="cover"
          style={{width: '100%', height: 250}}
        />
      </View>
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Image
          source={require('../../assets/GGsignButton.png')}
          resizeMode="contain"
          style={{width: 60, height: 60, paddingBottom: 30}}
        />
        <TouchableOpacity
          // style={styles.buttonText}
          onPress={signInWithGoogleAsync}>
          <LinearGradient
            colors={['#4c669f', '#3b5998', '#192f6a']}
            style={styles.linearGradient2}>
            <Text style={styles.textInButton}>Đăng nhập với Google</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  linearGradient: {
    height: 190,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    justifyContent: 'center',
  },
  buttonText: {
    color: '#ffffff',
    backgroundColor: '#00ffff',
    borderWidth: 1,
    borderRadius: 15,
  },
  textInButton: {
    fontSize: 22,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: 'white',
  },
  linearGradient2: {
    flex: 0.5,
    borderRadius: 15,
    textAlign: 'center',
    alignItems: 'center',
  },
});
