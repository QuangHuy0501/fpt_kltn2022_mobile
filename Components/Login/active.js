import {
  View,
  Text,
  Dimensions,
  Keyboard,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {TextInput} from 'react-native-paper';
import AntDesign from 'react-native-vector-icons/AntDesign';
import API from '../API/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {avtURL, LoginAppToken, userFullName} from '../Constant-Storage';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import OtpInputs from '../Login/index copy';

export default function ActiveUser({navigation}) {
  const ScreenWidth = Dimensions.get('screen').width;
  const ScreenHeight = Dimensions.get('screen').height;
  const [otpCode, setOtpCode] = useState('');
  const [keyboardStatus, setKeyboardStatus] = useState(false);

  useEffect(() => {
    const backAction = async () => {
      try {
        const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        // setloggedIn(false);
        // setuserInfo([]);
        await AsyncStorage.clear();
        navigation.navigate('LoginPage');
        BackHandler.removeEventListener('hardwareBackPress');
      } catch (err) {
        alert('loi log out' + err);
      }
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const activeCode = async () => {
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    const avatarLink = await AsyncStorage.getItem(avtURL);
    const userName = await AsyncStorage.getItem(userFullName);
    const otpReal = {otp: otpCode};
    //console.log(typeof(otpCode));
    //console.log(typeof(otpReal));
    const api = new API();
    api
      .onCallAPI(
        'put',
        'sys_users/activate',
        otpReal,
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(res => {
        if (res.data) {
          navigation.push('mainBoard', {
            avtURL: avatarLink,
            userName: userName,
          });
          BackHandler.removeEventListener('hardwareBackPress');
        }
      })
      .catch(err => console.log(err));
  };

  return (
    <View style={{flex: 1, position: 'relative'}}>
      <View
        style={{
          marginTop: ScreenHeight * 0.1,
          marginLeft: 20,
          marginRight: 10,
        }}>
        <Text style={{fontSize: 30, color: 'black', fontWeight: 'bold'}}>
          Nhập code OTP của bạn ở đây:
        </Text>
        <Text style={{fontSize: 20, fontWeight: '400'}}>
          Chúng tôi đã gửi mã OTP(gồm 4 số) đến mail của bạn, vui lòng kiểm tra:
        </Text>
      </View>
      <View style={{marginTop: 40, paddingHorizontal: 20}}>
        {/* <TextInput
          label="Nhập vào đây: (Vd: 1234)"
          style={{borderRadius: 20}}
          value={otpCode}
          onSubmitEditing={Keyboard.dismiss}
          onChangeText={otpCode => setOtpCode(otpCode)}
        /> */}
        <OtpInputs
          numberOfInputs={4} // pass any number as per requirements
          focusedBorderColor={'blue'}
          unFocusedBorderColor={'black'}
          clearTextOnFocus={true}
          errorMessage={'Invalid OTP'} // pass error message if applicable
          inputTextErrorColor={'black'}
          errorMessageTextStyles={{color: 'red'}} // Error message text style
          handleChange={code => {
            console.log('you code ==>', code);
            // Do your code
            setOtpCode(code)
          }}
          keyboardType={'number-pad'}
          secureTextEntry={false}
          inputStyles={{}} // Pass textinput style if applicable
          inputContainerStyles={{marginLeft: 20, marginRight: 20}} //Pass you style
        />
      </View>
      <View style={{marginTop: 60, alignItems: 'center', paddingTop: 100}}>
        <TouchableOpacity
          onPress={activeCode}
          style={{
            height: 50,
            width: 200,
            backgroundColor: '#ECA376',
            borderRadius: 20,
            elevation: 10,
            shadowOffset: {width: 5, height: 5},
            shadowColor: 'black',
            shadowOpacity: 1,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 25,
              color: 'white',
              textAlign: 'center',
            }}>
            Xác Nhận
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
