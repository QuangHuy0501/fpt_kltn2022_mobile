import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {LoginAppToken, majorUserChoose} from '../Constant-Storage';
import API from '../API/api';
import LinearGradient from 'react-native-linear-gradient';

export default function MajorResult({navigation, route}) {
  const ScreenHeight = Dimensions.get('screen').height;
  const ScreenWidth = Dimensions.get('screen').width;
  const [fullDescription, setFullDescription] = useState([]);
  const [isPressDescription, setIsPressDescription] = useState([]);
  const {group_id} = route.params;
  //console.log(group_id);
  const [majorNames, setMajorNames] = useState([]);
  // const group_id = 3;
  const majorPress = async id => {
    const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
    await AsyncStorage.setItem(majorUserChoose, id.toString());
    const api = new API();
    api
      .onCallAPI(
        'post',
        `sys_users/major`,
        {majorId: id},
        {},
        {Authorization: 'bearer ' + tokenDnhap},
      )
      .then(res => {
        if (res.data) {
          navigation.push('InputGrade', {
            majorID: id,
          });
        }
      })
      .catch(err => {
        navigation.push('InputGrade', {
          majorID: id,
        });
        alert(err);
      });
  };
  useEffect(() => {
    const getMajor = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      await api
        .onCallAPI(
          'get',
          `majors/personality_group/${group_id}`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(res => {
          if (res.data) {
            setMajorNames(res.data);
            const temp = [];
            const ispress = [];
            res.data.map(item => {
              temp.push(item.description);
              ispress.push(false);
            });
            setFullDescription(temp);
            setIsPressDescription(ispress);
          }
        })
        .catch(err => alert(err));
    };
    getMajor();
  }, []);
  const xemThemDescription = key => {
    const xemThem = '... Xem thêm';
    const hienThiBot = '... Hiển Thị bớt';
    const text = fullDescription[key];
    const isPress = isPressDescription[key];
    const tempIsPress = [...isPressDescription];
    if (text?.length > 50) {
      const temp = text.slice(0, 50);
      return (
        <Text
          style={{
            fontSize: 21,
            textAlign: 'center',
            paddingBottom: 20,
          }}>
          {isPress ? text : temp}
          <TouchableOpacity
            onPress={() => {
              console.log(!isPress);
              tempIsPress[key] = !isPress;
              setIsPressDescription(tempIsPress);
              // setSescription(text);
            }}>
            <Text
              style={{
                fontSize: 21,
                color: 'blue',
                fontWeight: 'bold',
              }}>
              {isPress ? hienThiBot : xemThem}
            </Text>
          </TouchableOpacity>
        </Text>
      );
    }
  };
  return (
    <View style={{flex: 1, position: 'relative', backgroundColor: '#BBDEFB'}}>
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: ScreenHeight * 0.15,
          width: ScreenWidth,
          borderBottomRightRadius: 20,
          borderBottomLeftRadius: 20,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          backgroundColor: 'white',
          marginBottom: 30,
        }}>
        <View style={{}}>
          <Text style={{fontSize: 30, color: 'white'}}>
            KẾT QUẢ NGHỀ NGHIỆP
          </Text>
        </View>
      </LinearGradient>
      <ScrollView
        style={{
          // justifyContent: 'center',
          // alignItems: 'center',
          height: ScreenHeight * 0.8,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          backgroundColor: 'white',
          marginLeft: 10,
          marginRight: 10,
          padding: 8,
          borderRadius: 10,
          flex: 1,
        }}>
        {/* <Text style={{fontSize: 20}}>Điểm bạn đạt được là {point}</Text>*/}
        <View style={{flex: 1, padding: 5}}>
          <Text
            style={{fontSize: 21, paddingBottom: 20, paddingHorizontal: 10}}>
            Đây là kết quả những ngành nghề phù hợp với bạn:{' '}
          </Text>
          {majorNames.map((item, index) => {
            // if (index < 4)
            return (
              <TouchableOpacity
                key={index}
                onPress={() => majorPress(item.majorId)}
                style={{
                  width: '100%',
                  backgroundColor: 'white',
                  elevation: 10,
                  shadowOffset: {width: 5, height: 5},
                  shadowColor: 'black',
                  shadowOpacity: 1,
                  borderRadius: 20,
                  marginBottom: 20,
                }}>
                <Text
                  key={index}
                  style={{
                    fontSize: 21,
                    textAlign: 'center',
                    paddingBottom: 20,
                    fontWeight: 'bold',
                  }}>
                  {item.majorName}:
                </Text>

                {xemThemDescription(index)}
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
      <View style={{height: 20}}></View>
    </View>
  );
}
