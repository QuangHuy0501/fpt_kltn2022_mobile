import {View, Text, Dimensions, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  currentStack,
  Current_Screen_Params,
  LoginAppToken,
} from '../Constant-Storage';
import API from '../API/api';
import LinearGradient from 'react-native-linear-gradient';
import {ScrollView} from 'react-native-gesture-handler';

export default function PersonalResult({navigation, route}) {
  const {testid} = route.params;
  const [personGrName, setPersonGrName] = useState();
  const ScreenHeight = Dimensions.get('screen').height;
  const ScreenWidth = Dimensions.get('screen').width;
  function maxByKey(array) {
    let max = array[0];
    array.map((item, index) => {
      if (item.averagePoint > max.averagePoint) {
        max = item;
      }
    });
    return max;
  }
  useEffect(() => {
    const getPersonGroupName = async () => {
      const tokenDnhap = await AsyncStorage.getItem(LoginAppToken);
      const api = new API();
      // console.log(`tests/result/${testid}`);
      await api
        .onCallAPI(
          'get',
          `tests/result/${testid}`,
          {},
          {},
          {Authorization: 'bearer ' + tokenDnhap},
        )
        .then(async res => {
          if (res.data) {
            //console.log(res.data);
            const groupMax = maxByKey(res.data);
            setPersonGrName(groupMax);

            //console.log(userPersonalID);
          }
        })
        .catch(err => alert(err));
    };
    getPersonGroupName();
  }, []);
  const userPersonalID = personGrName?.id;
  const goToJobSuggest = async () => {
    await AsyncStorage.setItem(currentStack, 'MajorResult');
    await AsyncStorage.setItem(
      Current_Screen_Params,
      userPersonalID.toString(),
    );

    navigation.navigate('MajorResult', {group_id: userPersonalID});
  };
  return (
    <View style={{flex: 1, position: 'relative', backgroundColor: '#BBDEFB'}}>
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: ScreenHeight * 0.15,
          width: ScreenWidth,
          borderBottomRightRadius: 20,
          borderBottomLeftRadius: 20,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          backgroundColor: 'white',
          marginBottom: 30,
        }}>
        <View style={{}}>
          <Text style={{fontSize: 30, color: 'white', fontFamily: 'tahoma'}}>
            KẾT QUẢ TÍNH CÁCH
          </Text>
        </View>
      </LinearGradient>

      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: ScreenHeight * 0.6,
          borderRadius: 20,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          backgroundColor: 'white',
          marginLeft: 10,
          marginRight: 10,
        }}>
        <Text style={{fontSize: 21, paddingBottom: 20, padding: 5}}>
          Đây là kết quả tính cách phù hợp với bạn:
        </Text>
        <Text
          style={{
            fontSize: 21,
            paddingBottom: 20,
            padding: 5,
            fontWeight: 'bold',
            textAlign: 'center',
          }}>
          {personGrName?.name}
        </Text>
        <View style={{height: ScreenHeight * 0.3}}>
          <ScrollView style={{}}>
            <Text style={{fontSize: 21, textAlign: 'center', padding: 5}}>
              {personGrName?.description}
            </Text>
          </ScrollView>
        </View>
      </View>
      <LinearGradient
        start={{x: 0.25, y: 0.5}}
        end={{x: 1.0, y: 1.0}}
        locations={[0, 0.5, 0.6]}
        colors={['#ECA376', '#F07122', '#EE8543']}
        style={{
          height: 50,
          width: 300,
          backgroundColor: '#00ffff',
          marginTop: 20,
          // position: 'absolute',
          // bottom: 10,
          borderRadius: 20,
          elevation: 10,
          shadowOffset: {width: 5, height: 5},
          shadowColor: 'black',
          shadowOpacity: 1,
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <TouchableOpacity onPress={goToJobSuggest} style={{}}>
          <Text
            style={{
              fontSize: 22,
              color: 'white',
              textAlign: 'center',
            }}>
            Xem nghề phù hợp với bạn
          </Text>
        </TouchableOpacity>
      </LinearGradient>
    </View>
  );
}
